(function(){
    var StripeBilling = {
        init: function(){
            this.form = $('#signupform');
            this.submitButton = this.form.find('input[type=submit]');
            this.submitButtonValue = this.submitButton.val();
            var stripeKey = $('meta[name="publishable-key"]').attr('content');
            Stripe.setPublishableKey(stripeKey);
            this.bindEvents();
        },
        bindEvents: function(){
            this.form.on('submit', $.proxy(this.sendToken, this));
        },
        sendToken: function(event){
            this.submitButton.val('One Moment').prop('disabled', true);
            Stripe.createToken(this.form, $.proxy(this.stripeResponseHandler, this));
            event.preventDefault();
        },
        stripeResponseHandler: function(status, response){
            console.log(status, response);
            if(response.error){
                this.form.find('.payment-errors').show().html(response.error.message);
                return this.submitButton.prop('disabled', false).val(this.submitButtonValue);
            }
            var token = response.id;
            console.log(token);
            this.form.append($('<input type="hidden" name="stripeToken" />').val(token));
            //this.form.get(0).submit();
        }
    };
    StripeBilling.init();
})();
