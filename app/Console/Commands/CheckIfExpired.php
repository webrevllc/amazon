<?php

namespace App\Console\Commands;

use App\Campaign;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CheckIfExpired extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pause_expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $campaigns = Campaign::get();
        $now = date('Y-m-d H:i', strtotime(Carbon::now()));
        foreach ($campaigns as $campaign) {
            $enddate = date('Y-m-d H:i', strtotime($campaign->end_date));
            if ($now == $enddate) {
                $campaign->status = "PAUSED";
                $campaign->save();
            }
        }
    }
}
