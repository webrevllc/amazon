<?php namespace App\Services;

use App\Contracts\Amazon;
use ApaiIO\Operations\Lookup;
use ApaiIO\ApaiIO;

class AmazonLookup implements Amazon{

    /**
     * @var ApaiIO
     */
    private $apaiIO;
    /**
     * @var Lookup
     */
    private $lookup;

    public function __construct(ApaiIO $apaiIO, Lookup $lookup)
{

    $this->apaiIO = $apaiIO;
    $this->lookup = $lookup;
}


    private function myLookup($asin)
    {
        $this->lookup->setItemId($asin);
        $this->lookup->setResponseGroup(array('Large'));
        $response = $this->apaiIO->runOperation($this->lookup);
        $xml = simplexml_load_string($response) or die("Error: Cannot create object");
        return $xml;

    }

    public function returnAmazonImages($asin)
    {
        $xml = $this->myLookup($asin);
        $urls = array();
        $images = $xml->Items->Item->ImageSets;

        try{
            foreach($images->ImageSet as $i){
                $url = $i->LargeImage->URL;
                $urls[] = $url;
            }
            return $urls;
        }catch (\ErrorException $e){
            return $this->returnAmazonImages('B00005UP2P');
        }

    }



    public function returnProductName($asin){
        $xml = $this->myLookup($asin);
        $brand = $xml;
        return $brand;
    }

    public function getDescription($asin){
        $xml = $this->myLookup($asin);
        $title = $xml->Items->Item->ItemAttributes;
        return $title;
    }

    public function getTitle($asin){
        $xml = $this->myLookup($asin);
        $title = $xml->Items->Item->ItemAttributes->Title;
        return $title;
    }

    public function getPrice($asin){
        $xml = $this->myLookup($asin);
        $title = $xml->Items->Item->Offers->Offer->OfferListing->Price->FormattedPrice;
        return $title;
    }

    public function returnFeatures($asin){
        $xml = $this->myLookup($asin);
        return $xml->Items->Item->ItemAttributes->Feature;
    }

    public function productUrl($asin){
        $xml = $this->myLookup($asin);
        return $xml->Items->Item->DetailPageURL;
    }
    public function reviews($asin){
        $xml = $this->myLookup($asin);
        return $xml->Items->Item->CustomerReviews->IFrameURL;
    }

    public function related($asin){
        $xml = $this->myLookup($asin);
        $related = [];
        $products =$xml->Items->Item->SimilarProducts->SimilarProduct;
        if($products){
            foreach($products as $product){
                $related[] = $this->myLookup($product->ASIN);
            }
        }else{
            $xml = $this->myLookup('B00005UP2P');
            $related = [];
            $products =$xml->Items->Item->SimilarProducts->SimilarProduct;
            foreach($products as $product){
                $related[] = $this->myLookup($product->ASIN);
            }
        }
        return $related;
    }
}