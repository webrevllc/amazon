<?php

namespace App\Services;
use App\Contracts\MailChimp;

class MailChimpConnector implements MailChimp
{
    private function makeUrl($apiKey)
    {
        $dc = substr($apiKey, strpos($apiKey, "-") + 1);
        return "https://$dc.api.mailchimp.com/3.0";
    }


    public function getLists($apiKey)
    {
        $url = $this->makeUrl($apiKey).'/lists/?count=1000';
        $ch     = curl_init();
        $headers = array( "Authorization: apikey $apiKey" );

        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
        $results = curl_exec( $ch );
        curl_close( $ch );
        $jsonResult = json_decode( $results, true);
        $listsArray = $jsonResult['lists'];
        return $listsArray;

    }

    public function addEmailToList($email, $listID, $apiKey)
    {
        $url = $this->makeUrl($apiKey).'/lists/'.$listID.'/members';
        $ch     = curl_init();
        $headers = array( "Authorization: apikey $apiKey" );
        $fields = ['email_address' => $email, 'status' => 'subscribed'];
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_POST, 1 );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($fields) );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
        $results = curl_exec( $ch );
        curl_close( $ch );
        $jsonResult = json_decode( $results, true);
        return $jsonResult;
    }

    public function addEmailAndUserToList($email, $firstName, $lastName, $listID, $apiKey)
    {
        $url = $this->makeUrl($apiKey).'/lists/'.$listID.'/members';
        $ch     = curl_init();
        $headers = array( "Authorization: apikey $apiKey" );
        $fields = ['email_address' => $email, 'status' => 'subscribed', 'merge_fields' => ["FNAME" => $firstName, "LNAME" => $lastName]];
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_POST, 1 );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($fields) );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
        $results = curl_exec( $ch );
        curl_close( $ch );
        $jsonResult = json_decode( $results, true);
        return $jsonResult;
    }
}