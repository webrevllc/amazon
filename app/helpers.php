<?php
use App\Campaign;
use App\Coupon;
use App\User;
use Carbon\Carbon;

function flash($title = null, $message = null)
{
    $flash = app('App\Http\Flash');

    if(func_num_args() == 0){
        return $flash;
    }
    return $flash->info($title, $message);
}

function set_active($path, $active = 'active') {

    return call_user_func_array('Request::is', (array)$path) ? $active : '';

}

function checkmail($myemail) {
    $domain = substr(strrchr($myemail, "@"), 1);

    $key = '06ae3978e7f0103909425a11b959613a';

    $request = 'http://check.block-disposable-email.com/easyapi/json/' . $key . '/' . $domain;

    $response = file_get_contents($request);

    $dea = json_decode($response);

    if ($dea->request_status == 'success') {
        if ($dea->domain_status == 'block') {
            //Access Denied
            return false;
        } else {
            // Access Granted
            return true;
        }
    } else {
        // something else went wrong with the address (maybe a malformed domain)
        return false;
    }
}

function allUsers(){
    return User::all();
}

function userBySubscription($subscription){
    return User::whereStripe_plan($subscription)->whereStripe_active(true)->get()->count();
}

function activeUsers(){
    return User::whereStripe_active(true)->get()->count();
}

function trialsEndingToday(){

    return User::whereStripe_active(true)
        ->whereBetween('trial_ends_at',[Carbon::today(), \Carbon\Carbon::now()])->get()->count();
}

function trialsEndingThisWeek(){
    return User::whereStripe_active(true)
        ->whereBetween('trial_ends_at',[Carbon::today(), \Carbon\Carbon::now()])->get()->count();
}

function campaignsCreatedToday(){
    return Campaign::whereBetween('created_at',[Carbon::today(), \Carbon\Carbon::now()])->get()->count();
}

function couponsClaimedToday(){
    return Coupon::whereBetween('updated_at',[Carbon::today()->startOfDay(), \Carbon\Carbon::now()])->get()->count();
}

function totalLiveCampaigns(){
    return Campaign::whereStatus('LAUNCHED')->get()->count();
}


