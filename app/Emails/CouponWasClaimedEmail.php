<?php

namespace App\Emails;

class CouponWasClaimedEmail extends Emails
{

    public function getEmailId()
    {
        return '3f0fb82e-1a34-44c2-8871-86fbbb7a1233';
    }

    public function variables($user, $email, $campaignName)
    {
        return [
            'name' => $user->name,
            'email' => $email,
            'campaign_name' => $campaignName,
        ];
    }
}

