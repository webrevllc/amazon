<?php


namespace App\Emails;


use CS_REST_Transactional_SmartEmail;

abstract class Emails
{
    /**
     * @var null
     */
    protected $apiKey;

    protected $data = [];

    /**
     * @param null $apiKey
     */
    public function __construct($apiKey = null)
    {

        $this->apiKey = $apiKey ?: config('services.campaign_monitor.key');
    }

    public function sendTo($user)
    {
        $mailer = $this->newTransaction();

        $data = call_user_func_array(
            [$this, 'variables'],
            array_merge(compact('user'), $this->data)
        );
        return $mailer->send([
            'To' => $user['email'],
            'Data' => $data
        ]);
    }

    public function newTransaction()
    {
        return new CS_REST_Transactional_SmartEmail(
            $this->getEmailId(), ['api_key' => $this->apiKey]
        );
    }

    protected abstract function getEmailId();


    public function withData($data)
    {
        $this->data = $data;
        return $this;
    }
}