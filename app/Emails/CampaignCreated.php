<?php

namespace App\Emails;

class CampaignCreated extends Emails
{

    public function getEmailId()
    {
        return '41a57b03-8a8e-49f6-bc1a-8751cac89d9d';
    }

    public function variables($user, $campaign)
    {
        return [
            'campaign' => ucwords(strtolower($campaign->campaign_name)),
            'campaign_url' => 'https://junglecoupon.com/coupon/'.$campaign->promo_url,
        ];
    }
}