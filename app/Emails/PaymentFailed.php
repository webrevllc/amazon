<?php

namespace App\Emails;

class PaymentFailed extends Emails
{

    public function getEmailId()
    {
        return '4379c0e6-265c-4fed-bcd0-5fc22502f2c2';
    }

    public function variables($user)
    {
        return [];
    }
}