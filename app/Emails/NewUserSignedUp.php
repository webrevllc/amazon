<?php

namespace App\Emails;

class NewUserSignedUp extends Emails
{

    public function getEmailId()
    {
        return 'a4981863-b052-4715-849a-e641ea369efc';
    }

    public function variables($user, $name, $email)
    {
        return [
            'name' => $name,
            'email' => $email,
        ];
    }
}

