<?php

namespace App\Emails;

class WelcomeEmail extends Emails
{
    public function getEmailId()
    {
        return 'e2762376-8a63-49e9-abd9-9ce3612e6268';
    }

    public function variables($user)
    {
        return [
            'name' => $user->name,
        ];
    }
}