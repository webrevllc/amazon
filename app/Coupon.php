<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $table = 'jungle_coupons';
    protected $fillable = ['campaign_id', 'coupon', 'assigned_to', 'campaign_name'];

    public function campaign()
    {
        return $this->belongsTo('App\Campaign');
    }
}
