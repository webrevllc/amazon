<?php

namespace App;

use App\Contracts\Amazon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Campaign extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'campaigns';
    protected $fillable = [
        'name',
        'begin_date',
        'end_date',
        'promo_title',
        'regular_price',
        'discount_price',
        'campaign_name',
        'promo_url',
        'img_url',
        'reviews',
        'pixel',
        'retargeting_pixel',
        'mc_api_key',
        'mc_list',
        'asin',
        'filter_emails',
        'filter_ips',
        'status',
        'claimed',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function coupons()
    {
        return $this->hasMany('App\Coupon');
    }

    public function sources()
    {
        return $this->hasMany('App\Source');
    }

    public function claims()
    {
        return $this->hasMany('App\Claim');
    }

    public function hasLaunched()
    {
        return $this->status == 'LAUNCHED';
    }

    public function pause()
    {
        return $this->status == 'PAUSED';
    }

    public function hasEnded(Amazon $amazon)
    {
        $relatedItems = $amazon->related($this->asin);
        return view('promo.ended2', compact('relatedItems'));
    }

    public function hasUrl()
    {
        return $this->promo_url != '';
    }

    public function hasTitle()
    {
        return $this->promo_title != '';
    }

    public function emailIsReady()
    {
        return $this->email_ready;
    }

    public function goForLaunch()
    {
        if($this->hasUrl() && $this->hasTitle() && $this->emailIsReady()){
            return true;
        }else{
            return false;
        }
    }
    public function ipFilterIsOn()
    {
        return $this->filter_ips;
    }

    public function emailFilterIsOn()
    {
        return $this->filter_emails;
    }
}
