<?php

namespace App\Listeners;

use App\Contracts\AweberContract;
use App\Events\CouponWasClaimed;
use AWeberAPIException;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddEmailToAweberList
{
    /**
     * @var AweberContract
     */
    private $aweberContract;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(AweberContract $aweberContract)
    {
        //
        $this->aweberContract = $aweberContract;
    }

    /**
     * Handle the event.
     *
     * @param  CouponWasClaimed  $event
     * @return void
     */
    public function handle(CouponWasClaimed $event)
    {
        $email = $event->email;
        $name = $event->firstName .' '. $event->lastName;
        if($event->campaign->aweber_list_id != ''){
            $account = $this->aweberContract->getAccount($event->campaign->aweber_key, $event->campaign->aweber_secret);
            try{
                if($event->campaign->collect_names){
                    $this->aweberContract->addEmailAndUserToList($account, $event->campaign->aweber_list_id, $email, $name);
                }else{
                    $this->aweberContract->addEmailToList($account, $event->campaign->aweber_list_id, $email);
                }

            }
            catch(AWeberAPIException $exc) {

            }
        }
    }
}
