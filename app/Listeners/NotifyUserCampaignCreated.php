<?php

namespace App\Listeners;

use App\Emails\WelcomeEmail;
use App\Events\CampaignCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use Snowfire\Beautymail\Beautymail;

class NotifyUserCampaignCreated
{
    /**
     * @var Beautymail
     */
    private $beautymail;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Beautymail $beautymail)
    {
        //
        $this->beautymail = $beautymail;
    }

    /**
     * Handle the event.
     *
     * @param  CampaignCreated  $event
     * @return void
     */
    public function handle(CampaignCreated $event)
    {
//        $user = $event->user;
//        $email = $event->user->email;
//        $campaign = $event->campaign;
//
//        $this->beautymail->send('emails.campaign_created', ['user' => $user, 'campaign' => $campaign], function ($m) use ($email){
//            $m->to($email)->subject('[JungleCoupon] New Campaign Created');
//            $m->bcc('srosenthal82@gmail.com')->subject('[JungleCoupon] New Campaign Created');
//        });

//        (new WelcomeEmail)->sendTo($event->user);
    }
}
