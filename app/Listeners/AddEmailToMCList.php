<?php

namespace App\Listeners;

use App\Contracts\MailChimp;
use App\Events\CouponWasClaimed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddEmailToMCList
{
    /**
     * @var MailChimp
     */
    private $mc;

    /**
     * Create the event listener.
     *
     * @param MailChimp $mc
     */
    public function __construct(MailChimp $mc)
    {
        $this->mc = $mc;
    }

    /**
     * Handle the event.
     *
     * @param  CouponWasClaimed  $event
     * @return void
     */
    public function handle(CouponWasClaimed $event)
    {
        $email = $event->email;
        $firstName = $event->firstName;
        $lastName = $event->lastName;
        $apiKey = $event->campaign->mc_api_key;
        $list_id = $event->campaign->mc_list;

        if($event->campaign->mc_api_key != ''){
            if($event->campaign->collect_names){
                $this->mc->addEmailAndUserToList($email, $firstName, $lastName, $list_id, $apiKey);
            }else{
                $this->mc->addEmailToList($email,$list_id,$apiKey);
            }
        }
    }
}
