<?php

namespace App\Listeners;

use App\Emails\WelcomeEmail;
use App\Events\UserSignedUp;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use Snowfire\Beautymail\Beautymail;

class SendUserWelcomeEmail
{

    /**
     * @param UserSignedUp $event
     */
    public function handle(UserSignedUp $event)
    {
//        $user = $event->user;
//        $email = $event->user->email;
//        $this->beautymail->send('emails.welcome', ['user' => $user], function ($m) use ($email){
//            $m->to($email)->subject('Welcome To Jungle Coupon');
//        });

        (new WelcomeEmail)->sendTo($event->user);
    }
}
