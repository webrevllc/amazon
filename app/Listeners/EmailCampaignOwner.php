<?php

namespace App\Listeners;

use App\Emails\CampaignCreated;
use App\Emails\WelcomeEmail;
use App\Events\CampaignLaunched;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Snowfire\Beautymail\Beautymail;

class EmailCampaignOwner
{

    /**
     * Handle the event.
     *
     * @param  CampaignLaunched  $event
     * @return void
     */
    public function handle(CampaignLaunched $event)
    {
        (new CampaignCreated)->withData(['campaign' => $event->campaign,])->sendTo($event->user);
        (new CampaignCreated)->withData(['campaign' => $event->campaign,])->sendTo(User::find(1));
    }
}
