<?php

namespace App\Listeners;

use App\Events\EmailWasReceivedForCoupon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use Snowfire\Beautymail\Beautymail;

class SendClaimToEmail
{
    /**
     * @var Beautymail
     */
    private $beautymail;

    /**
     * Create the event listener.
     *
     * @param Beautymail $beautymail
     */
    public function __construct(Beautymail $beautymail)
    {
        $this->beautymail = $beautymail;
    }

    /**
     * Handle the event.
     *
     * @param  EmailWasReceivedForCoupon  $event
     * @return void
     */
    public function handle(EmailWasReceivedForCoupon $event)
    {
        $email = $event->email;
        $campaign = $event->campaign;
        $claim = $event->claim;
        $this->beautymail->send('emails.coupon', ['campaign' => $campaign, 'claim' => $claim], function ($m)  use($campaign, $email){
            $m->from($campaign->email_address, $campaign->email_name);
            $m->to($email)->subject($campaign->email_subject);
        });
    }
}
