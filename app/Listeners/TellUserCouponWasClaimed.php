<?php

namespace App\Listeners;

use App\Emails\CouponWasClaimedEmail;
use App\Events\CouponWasClaimed;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use Snowfire\Beautymail\Beautymail;

class TellUserCouponWasClaimed
{

    /**
     * Handle the event.
     *
     * @param  CouponWasClaimed  $event
     * @return void
     */
    public function handle(CouponWasClaimed $event)
    {
//        $email = $event->email;
//        $user = User::find($event->campaign->user_id);
//
//        (new CouponWasClaimedEmail)->withData(['email' => $email, 'campaign' => $event->campaign->name])->sendTo($user);
    }
}
