<?php

namespace App\Listeners;

use App\Events\MessageReceived;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Snowfire\Beautymail\Beautymail;

class EmailMessageToShane
{
    /**
     * @var Beautymail
     */
    private $beautymail;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Beautymail $beautymail)
    {
        //
        $this->beautymail = $beautymail;
    }

    /**
     * Handle the event.
     *
     * @param  MessageReceived  $event
     * @return void
     */
    public function handle(MessageReceived $event)
    {
        $email = $event->request->email;
        $this->beautymail->send('emails.message_received', ['data' => $event->request], function ($m) use ($email){
            $m->to('support.33959.e3084bd7584bf7b8@helpscout.net')->subject('[JungleCoupon] Message Received')->from($email);
        });
    }
}
