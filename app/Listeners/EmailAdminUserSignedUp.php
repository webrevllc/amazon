<?php

namespace App\Listeners;

use App\Emails\NewUserSignedUp;
use App\Events\UserSignedUp;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Snowfire\Beautymail\Beautymail;

class EmailAdminUserSignedUp
{

    public function handle(UserSignedUp $event)
    {
        $user = $event->user;
        $email = $event->user->email;
        $shane = User::find(1);
        $steven = User::find(2);

        (new NewUserSignedUp)->withData(['name' => $user->name, 'email' => $email])->sendTo($shane);
        (new NewUserSignedUp)->withData(['name' => $user->name, 'email' => $email])->sendTo($steven);
    }
}
