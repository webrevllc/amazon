<?php

namespace App\Listeners;

use App\Contracts\MailChimp;
use App\Events\UserSignedUp;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddUserToMailchimpList
{
    /**
     * @var MailChimp
     */
    private $mailChimp;

    /**
     * Create the event listener.
     *
     * @param MailChimp $mailChimp
     */
    public function __construct(MailChimp $mailChimp)
    {
        //
        $this->mailChimp = $mailChimp;
    }

    /**
     * Handle the event.
     *
     * @param  UserSignedUp  $event
     * @return void
     */
    public function handle(UserSignedUp $event)
    {
        $email = $event->user->email;
        $apiKey = '0539d197fc668c55c04367a1f9817b1b-us3';
        $list_id = '927fdb9ab3';
        $this->mailChimp->addEmailToList($email,$list_id,$apiKey);
    }
}
