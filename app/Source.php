<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Source extends Model
{
    protected $table = 'sources';
    protected $fillable = ['campaign_id', 'url', 'count'];

    public function campaign()
    {
        return $this->belongsTo('App\Campaign');
    }
}
