<?php

namespace App\Contracts;

interface MailChimp
{

    public function getLists($apikey);
    public function addEmailToList($email, $listID, $apiKey);
    public function addEmailAndUserToList($email, $firstName, $lastName, $listID, $apiKey);

}