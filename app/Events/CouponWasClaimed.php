<?php

namespace App\Events;

use App\Campaign;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CouponWasClaimed extends Event
{
    use SerializesModels;
    /**
     * @var Campaign
     */
    public $campaign;
    /**
     * @var
     */
    public $email;
    /**
     * @var string
     */
    public $firstName;
    /**
     * @var string
     */
    public $lastName;

    /**
     * Create a new event instance.
     *
     * @param $campaign
     * @param $email
     * @param string $firstName
     * @param string $lastName
     */
    public function __construct($campaign, $email, $firstName, $lastName)
    {
        $this->campaign = $campaign;
        $this->email = $email;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
