<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class EmailWasReceivedForCoupon extends Event
{
    use SerializesModels;
    /**
     * @var
     */
    public $email;
    /**
     * @var
     */
    public $campaign;
    /**
     * @var
     */
    public $claim;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($email, $campaign, $claim)
    {
        //
        $this->email = $email;
        $this->campaign = $campaign;
        $this->claim = $claim;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
