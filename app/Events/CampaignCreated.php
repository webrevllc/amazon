<?php

namespace App\Events;

use App\Campaign;
use App\Events\Event;
use App\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CampaignCreated extends Event
{
    use SerializesModels;
    /**
     * @var User
     */
    public $user;
    /**
     * @var Campaign
     */
    public $campaign;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, Campaign $campaign)
    {
        $this->user = $user;
        $this->campaign = $campaign;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
