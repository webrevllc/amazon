<?php

namespace App\Events;

use App\Events\Event;
use App\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserPaid extends Event
{
    use SerializesModels;
    /**
     * @var User
     */
    public $user;
    /**
     * @var
     */
    public $plan;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, $plan)
    {
        //
        $this->user = $user;
        $this->plan = $plan;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
