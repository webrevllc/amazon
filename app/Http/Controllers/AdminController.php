<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

class AdminController extends Controller
{
    public function index()
    {
        if(Gate::denies('view_admin'))
        {
            abort(401, 'Sorry, you are not authorized to view this page!');
        }
        return view('admin.admin.index');
    }

    public function reports()
    {
        if(Gate::denies('view_admin'))
        {
            abort(401, 'Sorry, you are not authorized to view this page!');
        }
        return view('admin.admin.reports');
    }
}
