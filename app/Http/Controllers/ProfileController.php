<?php

namespace App\Http\Controllers;

use App\User;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;

class ProfileController extends Controller
{
//Route::post('/profile/account', 'ProfileController@accountUpdate');
//Route::post('/profile/subscription', 'ProfileController@subscriptionUpdate');
//Route::post('/profile/credit_card', 'ProfileController@ccUpdate');
//Route::get('/profile/cancel', 'ProfileController@cancel');

    public function accountUpdate(Request $request)
    {

        $v = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'password' => 'confirmed'
        ]);

       if($v->fails()){
           return redirect()->back()->withErrors($v->errors());
       }


        $user = Auth::user();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();
        flash()->success('YES', 'Profile updated!');
        return redirect()->back();
    }

    public function subscriptionUpdate(Request $request)
    {
        $user = Auth::user();
        $user->subscription($request->subscription)->swap();
        flash()->success('YES', 'Subscription changed!');
        return redirect()->back();
    }

    public function ccUpdate(Request $request)
    {
       \Auth::user()->updateCard($request->stripeToken);
        flash()->success('YES!', 'Card has been updated');
        return redirect()->back();
    }

    public function other(Request $request)
    {
        $user = User::find(Auth::user()->id);

        $user->mc_api_key = $request->api_key;
        $user->save();

        flash()->success('All set!', 'Now back to business...');
        return redirect()->back();
    }

    public function cancel()
    {
        $user = Auth::user();

        $user->subscription()->cancel();

        flash()->success('Sorry', 'It was fun while it lasted...');
        return redirect()->back();
    }

    public function coupon(Request $request)
    {
        $user = Auth::user();

        try {
            $user->applyCoupon($request->coupon);
            flash()->success('Coupon has been applied!','');
            return redirect()->back();
        } catch (Exception $e) {
            return back()->withErrors($e->getMessage());
        }
    }

    public function resubscribe(Request $request)
    {
        $user = \Auth::user();
        $user->subscription('JUNGLEMONTH')->resume($request->stripeToken);
        flash()->success('Welcome Back!', "Let's do this thing!");
        return redirect('/admin/campaigns');
    }
}
