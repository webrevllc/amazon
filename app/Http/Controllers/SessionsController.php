<?php

namespace App\Http\Controllers;
use App\Events\UserPaid;
use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Stripe\Stripe;



class SessionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        if (\Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            flash()->success('Welcome Back!', 'Nice to see you again');
            return redirect('/');
        }
        return flash()->error('Oops!', 'Something went wrong!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
    public function showSignUp()
    {
//        echo 'hi';
        return view('auth.signup');
    }

    public function subscribeUser(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $plan = $request->plan;
        if($request->has('coupon')){
            $user->subscription($plan)
                ->withCoupon($request->coupon)
                ->create($request->stripeToken);

        }else{
            $user->subscription($plan)
                ->create($request->stripeToken);
        }
        event(new UserPaid($user, $plan));
        return redirect('/conversion');
    }
}
