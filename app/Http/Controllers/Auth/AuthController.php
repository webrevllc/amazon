<?php

namespace App\Http\Controllers\Auth;

use App\Events\UserSignedUp;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Socialite;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $redirectPath = '/admin/';
//    protected $redirectTo = '/2';
    protected $loginPath = '/login';
    protected $redirectAfterLogout = '/login';
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
    public function getRegister(Request $request)
    {
        return view('auth.register');
    }

    public function postRegister(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
                $this->throwValidationException(
                    $request, $validator
            );
        }
        $user =$this->create($request->all());
        $plan = $request->plan;

        if($request->has('coupon')){
            $user->subscription($plan)
                ->withCoupon($request->coupon)
                ->create($request->stripeToken, ['email' => $request->email]);

        }else{
            $user->subscription($plan)
                ->create($request->stripeToken, ['email' => $request->email]);
        }
        Auth::login($user);
        event(new UserSignedUp($user));
        return redirect('/conversion');
    }

    public function postRegisterFB(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }
        $user = auth()->user();
        $plan = $request->plan;

        if($request->has('coupon')){
            $user->subscription($plan)
                ->withCoupon($request->coupon)
                ->create($request->stripeToken, ['email' => $request->email]);

        }else{
            $user->subscription($plan)
                ->create($request->stripeToken, ['email' => $request->email]);
        }
        event(new UserSignedUp($user));
        return redirect('/conversion');
    }

}
