<?php

namespace App\Http\Controllers;

use App\Claim;
use App\Campaign;
use App\Contracts\AweberContract;
use App\Contracts\MailChimp;
use App\Coupon;
use App\Events\CampaignLaunched;
use App\Http\Requests;
use App\Contracts\Amazon;
use App\Http\Requests\UpdateCampaignRequest;
use App\Source;
use App\User;
use Auth;
use Exception;
use Illuminate\Http\Request;
use App\Events\CampaignCreated;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Excel;

class CampaignController extends Controller
{

    /**
     * @var AweberContract
     */
    private $aweberContract;
    /**
     * @var MailChimp
     */
    private $mc;

    public function __construct(AweberContract $aweberContract, MailChimp $mc)
    {
        $this->middleware('customer');
        $this->aweberContract = $aweberContract;
        $this->mc = $mc;
    }

    /**
 * Display a listing of the resource.
 *
 * @return Response
 */
    public function index()
    {
        $total_campaigns = Campaign::where('user_id', \Auth::id())->get();
        $trashed_count = Campaign::where('user_id', \Auth::id())->onlyTrashed()->count();
        $campaigns = Campaign::where('status', 'LAUNCHED')->where('user_id', \Auth::id())->get();

//        $trashed = count(Campaign::where('status', 'LAUNCHED')->where('user_id', \Auth::id()));
        return view('admin.campaigns.all', compact('campaigns', 'total_campaigns', 'trashed_count'));
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function trashed()
    {
        $total_campaigns = Campaign::where('user_id', \Auth::id())->get();
        $trashed = Campaign::onlyTrashed()
            ->where('user_id', Auth::id())
            ->get();
        $trashed_count = count($trashed);
        return view('admin.campaigns.trashed', compact('trashed', 'total_campaigns', 'trashed_count'));
    }

    public function paused()
    {
        $total_campaigns = Campaign::where('user_id', \Auth::id())->get();
        $campaigns = Campaign::where('status', 'PAUSED')->where('user_id', \Auth::id())->get();
        $trashed_count = Campaign::where('user_id', \Auth::id())->onlyTrashed()->count();
        return view('admin.campaigns.all', compact('campaigns', 'total_campaigns', 'trashed_count'));
    }
    public function needs()
    {
        $total_campaigns = Campaign::where('user_id', \Auth::id())->get();
        $campaigns = Campaign::where('status', 'NEEDS COUPONS')->where('user_id', \Auth::id())->get();
        $trashed_count = Campaign::where('user_id', \Auth::id())->onlyTrashed()->count();
        return view('admin.campaigns.all', compact('campaigns', 'total_campaigns', 'trashed_count'));
    }

    public function ready()
    {
        $total_campaigns = Campaign::where('user_id', \Auth::id())->get();
        $campaigns = Campaign::where('status', 'READY TO LAUNCH')->where('user_id', \Auth::id())->get();
        $trashed_count = Campaign::where('user_id', \Auth::id())->onlyTrashed()->count();
        return view('admin.campaigns.all', compact('campaigns', 'total_campaigns', 'trashed_count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.campaigns.create', compact('campaigns'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request, Amazon $amazon)
    {
        $rules = [
            'campaign_name' => 'required',
            'asin' => 'required',
            'promo_url' => 'alpha_dash|unique:campaigns,promo_url',
            'end_date' => 'required',
        ];

        $this->validate($request, $rules);
        $user = \Auth::user();


        $campaign = new Campaign;
        $campaign->campaign_name = $request->campaign_name;
        $campaign->end_date = $request->end_date;
        $campaign->user_id = $user->id;
        $campaign->asin = trim($request->asin);
        $campaign->promo_url = $request->promo_url;

        try {
            if (! $amazon->getPrice($request->asin)) {
                throw new Exception("The field is undefined.");
            }
        }
        catch (Exception $e) {
            $campaign->status = 'NEEDS COUPONS';
            $user->campaigns()->save($campaign);
            event(new CampaignCreated($user, $campaign));
            flash($campaign->campaign_name, "Campaign created, but we could not find the price of that item!");
            return redirect('/admin/campaigns/'.$campaign->id.'/edit');
        }

        $campaign->regular_price = $amazon->getPrice($request->asin);
        $campaign->status = 'NEEDS COUPONS';
        $user->campaigns()->save($campaign);
        event(new CampaignCreated($user, $campaign));
        flash()->success($campaign->campaign_name, "Campaign created!");
        return redirect('/admin/campaigns/'.$campaign->id.'/edit');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id, Amazon $amazon)
    {

        //need to override the url, id
//        $campaign = Campaign::find($id);
        $campaign = Auth::user()->campaigns()->find($id);
        if(count($campaign->coupons) > 0){
            return view('admin.campaigns.show', compact('campaign', 'amazon'));
        }
        flash()->error('Oops', 'No coupons!');
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id, Amazon $amazon)
    {
        if($this->checkForASIN($id)){
            $campaign = Campaign::with('coupons')->find($id);
            if(Auth::user()->id == $campaign->user_id){  //CREATE A POLICY AUTHORIZATION
                return view('admin.campaigns.edit', compact('campaign', 'amazon'));
            }
            else{
                flash()->error('Oops!', "You can only edit your own campaigns");
                return redirect('/admin/campaigns');
            }
        }else{
            flash()->error('Oops', 'Something went wrong!');
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update($id, UpdateCampaignRequest $request)
    {
        $campaign = Campaign::find($id);
        $campaign->campaign_name = $request->campaign_name;
        $campaign->promo_title = $request->promo_title;
        $campaign->regular_price = $request->regular_price;
        $campaign->discount_price = $request->discount_price;
        $campaign->end_date = $request->end_date;
        $campaign->reviews = $request->has('reviews');
        $campaign->filter_ips = $request->has('filter_ips');
        $campaign->filter_emails = $request->has('filter_emails');
        $campaign->use_video = $request->has('use_video');
        $campaign->show_coupons = $request->has('show_coupons');
        $campaign->collect_names = $request->has('collect_names');
        $campaign->super_url = $request->super_url;
        $campaign->save();
        flash()->success('Campaign Updated', 'Have you selected your featured image yet?');
        return redirect()->back();
    }

    public function uploadPhoto(Request $request, $id)
    {
        $campaign = Campaign::find($id);
        $image = $request->file('image');
        $name = time() . $image->getClientOriginalName();
        $image->move('campaigns/photos', $name);
        $campaign->imageURL = $name;
        $campaign->save();
    }

    private function checkForASIN($id)
    {
        $campaign = Campaign::find($id);
        if($campaign->asin != ''){
            return true;
        }else{
            return false;
        }
    }

    public function selectList(Request $request)
    {
        $id = $request->lists;
        $campaign = Campaign::find($request->campaignId);
        $campaign->mc_list = $id;
        $campaign->save();
        flash()->success('HEY!', 'Nice list :)');
        return redirect()->back();
    }


    public function launch($id)
    {

        $campaign = Campaign::find($id);
        if($campaign->goForLaunch()){
            $campaign->status = 'LAUNCHED';
            $campaign->save();
            event(new CampaignLaunched(Auth::user(), $campaign));
            flash()->overlay($campaign->campaign_name .' has launched', '');
            return redirect()->back();
        }else{
            flash()->overlay($campaign->campaign_name .' has issues...', 'Check coupons, promo title and promo url, did you set up the new email feature?', 'error');
            return redirect('/admin/campaigns/'. $campaign->id.'/edit');
        }

    }

    public function verifyUrl(Request $request)
    {
        $userCampaigns = Auth::user()->campaigns;
        foreach($userCampaigns as $campaign){
            if($request->url == $campaign->promo_url){
                return 'true';
            }else{
                $c2 = Campaign::where('promo_url','=', $request->url)->get();
                if($c2->isEmpty()){
                    return 'true'; //verified show green
                }else{
                    return 'false'; //show red
                }
            }
        }
        return 'true';
    }

    public function destroy($id)
    {
        Campaign::destroy($id);
        return redirect()->back();
    }

    public function restore($id)
    {
        $campaign = Campaign::withTrashed()->find($id);
        $campaign->restore();
        return redirect()->back();
    }

    public function pause($id)
    {
        $campaign = Campaign::find($id);
        $campaign->status = "PAUSED";
        $campaign->save();
        return redirect()->back();
    }

    public function images($id, Amazon $amazon)
    {
        $campaign = Campaign::find($id);
        try {
            $images = $amazon->returnAmazonImages($campaign->asin);
        } catch (Exception $e) {
            $images = [];
        }

        return view('admin.campaigns.images', compact('campaign', 'images'));
    }
    public function tracking($id)
    {
        $campaign = Campaign::find($id);
        return view('admin.campaigns.tracking', compact('campaign'));
    }
    public function coupons($id)
    {
        $campaign = Campaign::find($id);
        return view('admin.campaigns.coupons', compact('campaign'));
    }



    public function setImage($id, Request $request)
    {
        $campaign = Campaign::find($id);
        $imageurl = $request->img_url;
        $campaign->img_url = $imageurl;
        $campaign->save();

        flash()->success('Featured Image Selected!', 'You are on a roll!');
        return redirect()->back();
    }

    public function addTracking($id, Request $request)
    {
        $campaign = Campaign::find($id);
        $campaign->retargeting_pixel = $request->retargeting_pixel;
        $campaign->pixel = $request->pixel;
        $campaign->analytics = $request->analytics;
        $campaign->save();
        return redirect()->back();
    }

    public function getCSV($id, Excel $excel)
    {
        $excel->create('Filename', function($e) use ($id){
            $e->sheet('Sheetname', function($sheet) use ($id) {
                $coupons = Coupon::where('campaign_id', $id)->get();
                $sheet->fromArray($coupons);

            });
        })->download('xls');
    }

    public function getCSV2(Excel $excel)
    {
        $excel->create('Filename', function($e){
            $e->sheet('Sheetname', function($sheet){
                $users = User::all();
                $sheet->fromArray($users);

            });
        })->download('xls');
    }
    public function lists($id)
    {
        $campaign = Campaign::find($id);
        if($campaign->mc_api_key != '')
        {
            if($this->mc->getLists($campaign->mc_api_key)){
                $mclists = $this->mc->getLists($campaign->mc_api_key);
            }else{
                $mclists = [];
            }
        }else
        {
            $mclists = '';
        }
        return view('admin.campaigns.lists.mc', compact('campaign', 'mclists'));
    }

    public function setMCAPI($id, Request $request)
    {
        $campaign = Campaign::find($id);
        $campaign->mc_api_key = trim($request->mc_api_key);
        $campaign->save();

        return redirect()->back();
    }

    public function setMCList($id, Request $request)
    {
        $campaign = Campaign::find($id);
        $mc_list = $request->mc_list;
        $campaign->mc_list = $mc_list;
        $campaign->save();

        return redirect()->back();
    }

    public function awebershow($id)
    {
        $campaign = Campaign::find($id);
        if($campaign->aweber_key != '')
        {
            $lists = $this->getAweberLists($id);
        }else{
            $lists = [];
        }

        return view('admin.campaigns.lists.aweber', compact('campaign', 'lists'));
    }

    public function activateAweber($campaign_id)
    {
        $ksaid = $this->aweberContract->display_access_tokens();
        if(!empty($ksaid))
        {
            $c = Campaign::find($campaign_id);
            $c->aweber_account_id = $ksaid['account_id'];
            $c->aweber_key = $ksaid['key'];
            $c->aweber_secret = $ksaid['secret'];
            $c->save();
            $this->getAweberLists($campaign_id);
        }
        Session::regenerateToken();
        return redirect('/admin/campaigns/'.$campaign_id.'/lists/aweber');
    }

    public function getAweberLists($id)
    {
        $campaign = Campaign::find($id);
        $account = $this->aweberContract->getAccount($campaign->aweber_key, $campaign->aweber_secret);
        $lists = $this->aweberContract->display_available_lists($account);
        return $lists;
    }

    public function setAweberList($id, Request $request)
    {
        $campaign = Campaign::find($id);
        $campaign->aweber_list_id = $request->aweber;
        $campaign->save();

        flash()->success('You did it!', 'Aweber List Saved!!');
        return redirect()->back();
    }

    public function email($id)
    {
        $campaign = Campaign::find($id);
        return view('admin.campaigns.email', compact('campaign'));
    }

    public function addEmail($id, Request $request)
    {
        $this->validate($request, [
            'email_name' => 'required',
            'email_address' => 'required',
            'email_subject' => 'required',
            'campaign_email_body' => 'required',
        ]);

        $campaign = Campaign::find($id);
        $campaign->email_name = $request->email_name;
        $campaign->email_address = $request->email_address;
        $campaign->email_subject = $request->email_subject;
        $campaign->facebook = $request->facebook;
        $campaign->twitter = $request->twitter;
        $campaign->campaign_email_body = $request->campaign_email_body;
        $campaign->email_ready = true;
        $campaign->save();

        flash()->success('Email Info', 'Successfully saved your email info!');
        return redirect()->back();
    }

    public function logo($id)
    {
        $campaign = Campaign::find($id);
        return view('admin.campaigns.logo', compact('campaign'));
    }
    public function emaillogo($id, Request $request)
    {
        $this->validate($request, [
            'logo' => 'required|mimes:jpg,jpeg,png,bmp'
        ]);

        $campaign = Campaign::findorfail($id);
        $file = $request->file('logo');
        $name = time() . $file->getClientOriginalName();
        Storage::disk('s3')->put($name, file_get_contents($file), 'public');
        $campaign->campaign_logo_url = $this->getFilePathAttribute($name);
        $campaign->save();
        return 'Done';
    }


    public function getFilePathAttribute($value)
    {

        $disk = Storage::disk('s3');
        $bucket = Config::get('filesystems.disks.s3.bucket');
        if ($disk->exists($value)) {
//            $command = $disk->getDriver()->getAdapter()->getClient()->getCommand('GetObject', [
//                'Bucket'                     => Config::get('filesystems.disks.s3.bucket'),
//                'Key'                        => $value,
//                'ResponseContentDisposition' => 'attachment;'
//            ]);

            $request = $disk->getDriver()->getAdapter()->getClient()->getObjectUrl($bucket, $value);

            return (string) $request;
        }

        return $value;
    }

    public function video($id, Request $request)
    {
        $campaign = Campaign::find($id);
        $campaign->videoID = $request->videoID;
        $campaign->save();
        flash()->success('Video Saved', 'Check out the preview');
        return redirect()->back();
    }

    public function getVideo($id)
    {
        $campaign = Campaign::find($id);
        return view('admin.campaigns.video', compact('campaign'));
    }


    public function pausedPage($id)
    {
        $campaign = Campaign::find($id);
        if($campaign->mc_api_key != '')
        {
            if($this->mc->getLists($campaign->mc_api_key)){
                $mclists = $this->mc->getLists($campaign->mc_api_key);
            }else{
                $mclists = [];
            }
        }else
        {
            $mclists = '';
        }
        return view('admin.campaigns.paused', compact('campaign', 'mclists'));
    }

    public function showSources($id)
    {
        $campaign = Campaign::with('sources')->find($id);
        return view('admin.campaigns.sources', compact('campaign'));
    }

    public function postSources($id, Request $request)
    {
        $campaign = Campaign::find($id);
        $qty = $request->qty;
        for($i=1;$i<=$qty;$i++){
            $source = new Source;
            $source->campaign_id = $id;
            $source->url = 'https://junglecoupon.com/coupon/'.$campaign->promo_url.'?source='.$i;
            $source->save();
        }
        flash()->success('You did it!', "Go make those ads!");
        return redirect()->back();

    }
}
