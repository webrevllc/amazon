<?php

namespace App\Http\Controllers;

use App\Claim;
use App\Contracts\AweberContract;
use App\Contracts\MailChimp;
use App\Coupon;
use App\Campaign;
use App\Contracts\Amazon;
use App\Source;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Events\CouponWasClaimed;
use App\Events\EmailWasReceivedForCoupon;

class ClaimController extends Controller
{

    /**
     * @var MailChimp
     */
    private $mc;
    /**
     * @var AweberContract
     */
    private $aweberContract;
    /**
     * @var Amazon
     */
    private $amazon;

    /**
     * ClaimController constructor.
     *
     * @param MailChimp $mc
     * @param AweberContract $aweberContract
     * @param Amazon $amazon
     */
    public function __construct( MailChimp $mc, AweberContract $aweberContract, Amazon $amazon ) {

        $this->mc = $mc;
        $this->aweberContract = $aweberContract;
        $this->amazon = $amazon;
    }
    public function validateClaimRequest($campaign, $request)
    {
        $countEmails = count($campaign->coupons->where('assigned_to', $request->email));
        $countIps = count($campaign->coupons->where('assigned_to_ip', $request->ip()));
        $email = $request->email;
        if($this->checkIps($campaign, $countIps) && $this->checkEmails($campaign, $countEmails, $email)){
            return true;
        }else{
            return false;

        }
    }

    public function checkIps($campaign, $ipCount)
    {
        if($campaign->filter_ips){
            if($ipCount == 0){
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }
    }

    public function checkEmails($campaign, $emailCount, $email)
    {
        if($emailCount == 0){
            if($campaign->filter_emails){
                if(checkmail($email)){
                    return true;
                }else{
                    return false;
                }
            }else{
                return true;
            }
        }else{
            return true;
        }
    }

    public function sendClaim($campaignID, Request $request)
    {
        $campaign = Campaign::with('coupons')->find($campaignID);
        if(count($campaign->coupons) == $campaign->claimed){
            $this->addEmailToListAnyway($campaign, $request);
            $relatedItems = $this->amazon->related($campaign->asin);
            return view('promo.ended2', compact('relatedItems'));
        }else{
            if($this->validateClaimRequest($campaign, $request)){
                if($request->has('source')){
                    $source = $request->source;
                }else{
                    $source = 'none';
                }
                $claim = $this->createClaimCode($campaignID, $request, $source);
                event(new EmailWasReceivedForCoupon($request->email, $campaign, $claim));
                return view('promo.coupon_sent', compact('campaign', 'claim'));
            }else{
                flash()->overlay('Sorry', 'Only 1 coupon per campaign per user per ip, no disposable emails allowed', 'error');
                return redirect()->back();
            }
        }
    }

    public function createClaimCode($campaignID, $request, $source)
    {
        $claim = new Claim;
        $claim->email = $request->email;
        if($request->first_name){
            $claim->first_name = $request->first_name;
            $claim->last_name = $request->last_name;
        }
        $claim->code = uniqid('jc'.rand(1,10000));
        $claim->campaign_id = $campaignID;
        $claim->source = $source;
        $claim->save();
        return $claim;
    }

    public function lookupClaim($claim_code, Amazon $amazon, Request $request)
    {
        try{
            $claim = Claim::with('campaign')->whereCode($claim_code)->firstorfail();
            $campaign = $claim->campaign;
            $coupon = $this->getTheCoupon($claim, $campaign, $request);
            if($coupon){
                try{
                    $sources = Source::where('campaign_id', $campaign->id)->get();
                    foreach($sources as $source){
                        if(substr($source->url, -1) == $claim->source){
                            $source->count++;
                            $source->save();
                        }
                    }
                }catch (ModelNotFoundException $m){

                }
                return view('promo.coupon',['coupon' => $coupon->coupon, 'amazon' => $amazon, 'campaign' => $campaign]);
            }else{
                $relatedItems = $amazon->related($campaign->asin);
                return view('promo.ended2', compact('relatedItems'));
            }
        }catch (ModelNotFoundException $m){
            $relatedItems = $amazon->related('B015GOLOFO');
            return view('promo.ended2', compact('relatedItems'));
        }

    }

    private function getTheCoupon($claim, $campaign, $request)
    {
        if(! $claim->is_claimed){
            $coupon = $this->getNextCoupon($claim, $request);
            if($coupon){
                event(new CouponWasClaimed($campaign, $claim->email, $claim->first_name, $claim->last_name));
            }else{
                return false;
            }
        }else{
            $coupon = $this->getThisClaimsCoupon($campaign, $claim->email);
        }
        return $coupon;
    }

    public function getNextCoupon($claim, $request)
    {
        try{
            $coupon = Coupon::whereCampaign_id($claim->campaign_id)->whereAssigned_to('')->firstorfail();
            $campaign = $claim->campaign;
            $coupon->assigned_to = $claim->email;
            $coupon->assigned_to_ip = $request->ip();
            $campaign->claimed ++;
            $claim->is_claimed = true;
            $coupon->save();
            $campaign->save();
            $claim->save();
            return $coupon;
        }catch(ModelNotFoundException $m){
            return false;
        }

    }

    private function getThisClaimsCoupon($campaign, $email)
    {
        try{
            $coupon = Coupon::where('campaign_id', $campaign->id)
                            ->where('assigned_to', $email)->firstorfail();

            return $coupon;
        }catch(ModelNotFoundException $m){
            return false;
        }

    }

//    private function emailHasClaimedCampaign($email, $campaign)
//    {
//        $claims = Claim::whereCampaign_id($campaign->id)->get();
//        $matched = array();
//        foreach ($claims as $c){
//            if ($c->email == $email){
//                $matched[] = $c->email;
//            }
//        }
//        return $matched;
//    }

    public function addEmailToListAnyway( $campaign, $request )
    {
        $email = $request->email;
        $apiKey = $campaign->mc_api_key;
        $list_id = $campaign->mc_list;

        if($campaign->mc_api_key != ''){
            $this->mc->addEmailToList($email,$list_id,$apiKey);
        }elseif($campaign->aweber_list_id != ''){
            $account = $this->aweberContract->getAccount($campaign->aweber_key, $campaign->aweber_secret);
            $this->aweberContract->addEmailToList($account, $campaign->aweber_list_id, $email);
        }else{

        }
    }

    public function resendClaim($claim_id ) {
        $claim = Claim::find($claim_id);
        $campaign = Campaign::find($claim->campaign_id);
        $email = $claim->email;
        event(new EmailWasReceivedForCoupon($email, $campaign, $claim));
        flash()->success('Success!', 'Another email has been sent!');
        return redirect()->back();
    }
}
