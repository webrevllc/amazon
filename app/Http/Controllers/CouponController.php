<?php

namespace App\Http\Controllers;

use App\Campaign;
use App\Contracts\Amazon;
use App\Coupon;
use App\MailChimp3;
use App\Source;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Requests;
use Input;


class CouponController extends Controller
{
    public function store($id, Request $request)
    {
        $campaign = Campaign::find($id);
        if(count($campaign->coupons) > 0){
            $first = false;
        }else{
            $first = true;
        }
        $file = $request->file('coupons');
        $file = fopen($file, "r");

        $count = 0;
        while(!feof($file)){
            $line = fgets($file);
            if($line != ''){
                $coupon = new Coupon;
                $coupon->coupon = $line;
                $count++;
                $campaign->coupons()->save($coupon);
            }
        }
        fclose($file);
        if($first){
            $campaign->status = "READY TO LAUNCH";
            $campaign->save();
        }
        flash()->success('You Did It!', $count . ' coupons have been added!');
        return redirect()->back();
    }

    public function show($id)
    {
        $campaign = Campaign::with('coupons', 'sources')->find($id);
        //if()  check if campaign belongs to user
        return view('admin.coupon', compact('campaign'));
    }

    public function lookupCoupon($promo_url, Amazon $amazon, Request $request)
    {

        try{
            $campaigner = Campaign::where('promo_url', $promo_url)->firstorfail();
            $couponCount = Coupon::where('campaign_id', $campaigner->id)->get()->count();
            if(Input::has('source')){
                $sources = Source::where('campaign_id', $campaigner->id)->get();
                foreach($sources as $source){
                    if(substr($source->url, -1) == Input::get('source')){
                        $source->visits++;
                        $source->save();
                    }
                }
            }

            if($couponCount == $campaigner->claimed){
                $relatedItems = $amazon->related($campaigner->asin);
                return view('promo.ended2', compact('relatedItems'));
            }else{
                try{
                    $campaign = Campaign::where('promo_url', $promo_url)->firstorfail();
                    if($campaign != null){
                        if($campaign->status == 'LAUNCHED'){
                            return view('promo.show', ['campaign' => $campaign, 'amazon' => $amazon]);
                        }
                        $relatedItems = $amazon->related($campaign->asin);
                        return view('promo.ended2', compact('relatedItems'));
                    }
                    $relatedItems = $amazon->related($campaign->asin);
                    return view('promo.ended2', compact('relatedItems'));
                }catch(ModelNotFoundException $m){
                    $relatedItems = $amazon->related('B00005UP2P');
                    return view('promo.ended2', compact('relatedItems'));
                }
            }
        }catch(ModelNotFoundException $m){
            $relatedItems = $amazon->related('B00005UP2P');
            return view('promo.ended2', compact('relatedItems'));
        }

    }

    public function deleteOne()
    {
        $coupon = Coupon::with('campaign')->find(Input::query('id'));
        if($coupon->campaign->user_id != \Auth::id()){
            flash()->error('Sorry', 'That is not yours!');
            return redirect()->back();
        }else{
           --$coupon->campaign->claimed;
            $coupon->campaign->save();
            $coupon->delete();
//            flash()->success('Sorry', 'That is not yours!');
            return redirect()->back();
        }
    }

    public function deleteAll()
    {
        $coupons = Coupon::where('campaign_id', Input::query('id'))->get();
        foreach($coupons as $coupon){
            if($coupon->campaign->user_id != \Auth::id()){
                flash()->error('Sorry', 'That is not yours!');
                return redirect()->back();
            }else{
                $coupon->campaign->claimed = 0;
                $coupon->campaign->save();
                $coupon->delete();
//            flash()->success('Sorry', 'That is not yours!');

            }
        }
        flash()->success('Success', 'All coupons successfully removed.');
        return redirect()->back();
    }
}
