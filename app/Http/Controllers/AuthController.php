<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use App\Http\Requests;
use Laravel\Socialite\Contracts\Factory as Socialite;

use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    protected $socialite;

    /**
     * AuthController constructor.
     * @param Socialite $socialite
     */
    public function __construct(Socialite $socialite){
        $this->socialite = $socialite;
    }


    public function getSocialAuth($provider=null)
    {
        if(!config("services.$provider")) abort('404'); //just to handle providers that doesn't exist

        return $this->socialite->with($provider)->redirect();
    }


    public function getSocialAuthCallback($provider=null)
    {
        if($user = $this->socialite->with($provider)->user()){
            try{
                $ourUser = User::where('email', $user->email)->firstOrFail();
                \Auth::login($ourUser);
                return redirect('/admin');
            }catch(ModelNotFoundException $m){
                $u = new User;
                $u->name = $user->name;
                $u->email = $user->email;
                $u->save();
                auth()->login($u);
                return view('auth.register-fb', compact('u'));
            }

        }else{
            return 'something went wrong';
        }
    }
}
