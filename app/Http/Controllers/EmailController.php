<?php

namespace App\Http\Controllers;

use App\Events\MessageReceived;
use App\MarketingEmail;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class EmailController extends Controller
{
    public function add(Request $request)
    {
        $marketing = new MarketingEmail;
        $marketing->email_address = $request->email;
        $marketing->save();
        flash()->success('Thanks', 'You have been added to the list!');
        return redirect()->back();
    }

    public function message(Request $request)
    {
        event(new MessageReceived($request));
        flash()->success('Thanks', 'Your message has been sent!');
        return redirect()->back();
    }
}
