<?php

namespace App\Http\Controllers;

use App\Emails\PaymentFailed;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Cashier\WebhookController as BaseController;
use Symfony\Component\HttpFoundation\Response;

class WebhookController extends BaseController
{
	/**
	 * Handle a stripe webhook.
	 *
	 * @param  array  $payload
	 * @return Response
	 */
	public function handleInvoicePaymentFailed($payload)
	{
		try{
			$user = User::where('stripe_id', $payload['data']['object']['customer'])->firstorfail();
			(new PaymentFailed)->sendTo($user);

		}catch(ModelNotFoundException $m){

		}

		return new Response('Webhook Handled', 200);
	}
}