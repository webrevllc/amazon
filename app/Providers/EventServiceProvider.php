<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\UserSignedUp' => [
            'App\Listeners\SendUserWelcomeEmail',
            'App\Listeners\EmailAdminUserSignedUp',
            'App\Listeners\AddUserToMailchimpList',
        ],

        'App\Events\CampaignCreated' => [
            'App\Listeners\NotifyUserCampaignCreated',
        ],
        'App\Events\CouponWasClaimed' => [
            'App\Listeners\TellUserCouponWasClaimed',
            'App\Listeners\AddEmailToMCList',
            'App\Listeners\AddEmailToAweberList',
        ],
        'App\Events\EmailWasReceivedForCoupon' => [
            'App\Listeners\SendClaimToEmail',
        ],
        'App\Events\CampaignLaunched' => [
            'App\Listeners\EmailCampaignOwner',
        ],
        'App\Events\MessageReceived' => [
            'App\Listeners\EmailMessageToShane',
        ],

    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
