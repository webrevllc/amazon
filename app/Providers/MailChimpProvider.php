<?php

namespace App\Providers;

use App\Contracts\MailChimp;
use App\Services\MailChimpConnector;
use Illuminate\Support\ServiceProvider;

class MailChimpProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(MailChimp::class, function(){
           return new  MailChimpConnector();
        });
    }
}
