<?php

namespace App\Providers;

use ApaiIO\ApaiIO;
use ApaiIO\Operations\Lookup;
use App\Services\AmazonLookup;
use Illuminate\Support\ServiceProvider;
use App\Contracts\Amazon;
use \ApaiIO\Configuration\GenericConfiguration;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
       $this->app->singleton(Amazon::class, function(){
           $conf = new \ApaiIO\Configuration\GenericConfiguration;
           $conf
               ->setCountry('com')
               ->setAccessKey(env('AMAZON_ACCESS_KEY'))
               ->setSecretKey(env('AMAZON_SECRET_KEY'))
               ->setAssociateTag(env('AMAZON_ASSOCIATE_TAG'));
           $lookup = new Lookup;
           $apaio = new ApaiIO($conf);

           return new  AmazonLookup($apaio, $lookup);
       });

    }
}
