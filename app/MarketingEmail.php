<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketingEmail extends Model
{
    protected $table = 'marketingemails';
    protected $fillable = ['email_address'];
}
