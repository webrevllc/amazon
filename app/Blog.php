<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable = ['title', 'category', 'body', 'excerpt'];
    protected $table = 'blogs';

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
