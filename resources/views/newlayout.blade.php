<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="publishable-key" content="{{env('STRIPE_KEY')}}">
    <!-- SEO -->
    <meta name="author" content="SDR">
    <meta name="description" content="Finally a way to easily distribute your Amazon single use coupons!">
    <meta name="keywords" content="Amazon, FBA, Private Label, Amazon Coupons">

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- Page Title -->
    <title>JungleCoupon</title>

    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Google Font : Open Sans & Montserrat -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,600' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

    <!-- Plugins -->
    <link href="/css/libs.css" rel="stylesheet">
    <link href="/css/plugins/animate.css" rel="stylesheet">
    <link href="/css/plugins/slick.css" rel="stylesheet">
    <link href="/css/plugins/magnific-popup.css" rel="stylesheet">
    <link href="/css/plugins/font-awesome.css" rel="stylesheet">
    <link href="/css/plugins/streamline-icons.css" rel="stylesheet">

    <!-- Event Style -->
    <link href="/css/event.css" rel="stylesheet">

    <!-- Color Theme -->
    <!-- Options: green, purple, red, yellow, mint, blue, black  -->
    <link href="/css/themes/mint.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="/js/ie/respond.min.js"></script>
    <![endif]-->

    <!-- Modernizr -->
    <script src="/js/modernizr.min.js"></script>
    <!-- Subtle Loading bar -->
    <script src="/js/plugins/pace.js"></script>
    @include('tracking')
    <style>
        .navbar-default{
            box-shadow: 0 2px 1px 0 red!important;
        }
    </style>
    @yield('pixel')
    <!-- Fixed navbar -->
    <script src="//static.getclicky.com/js" type="text/javascript"></script>
    <script type="text/javascript">try{ clicky.init(100875616); }catch(e){}</script>
    <noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/100875616ns.gif" /></p></noscript>
</head>

<body class="animate-page" data-spy="scroll" data-target="#navbar" data-offset="100">
<!--Preloader div-->
{{--<div class="preloader"></div>--}}



@yield('content')

<!--
 Javascripts : Nerd Stuff :)
 ====================================== -->

<!-- jQuery Library -->
<script src="/js/jquery.min.js"></script>

<!-- Bootstrap JS -->
<script src="/js/bootstrap.min.js"></script>

<!-- 3rd party Plugins -->
<script src="/js/plugins/countdown.js"></script>
<script src="/js/plugins/wow.js"></script>
<script src="/js/plugins/slick.js"></script>
<script src="/js/plugins/magnific-popup.js"></script>
<script src="/js/plugins/validate.js"></script>
<script src="/js/plugins/appear.js"></script>
<script src="/js/plugins/count-to.js"></script>
<script src="/js/plugins/nicescroll.js"></script>


<!-- JS Includes -->

<!-- Main Script -->
<script src="/js/main.js"></script>

<script src="/js/libs.js"></script>
@include('flash')
@yield('footer')
</body>

</html>
