<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="publishable-key" content="{{env('STRIPE_KEY')}}">
    <!-- SEO -->
    <meta name="author" content="JungleCoupon.com">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- Page Title -->
    <title>JungleCoupon</title>

    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Google Font : Open Sans & Montserrat -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,600' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

    <!-- Plugins -->
    {{--<link href="/css/plugins/animate.css" rel="stylesheet">--}}
    {{--<link href="/css/plugins/slick.css" rel="stylesheet">--}}
    {{--<link href="/css/plugins/magnific-popup.css" rel="stylesheet">--}}
    <link href="/css/plugins/font-awesome.css" rel="stylesheet">
    {{--<link href="/css/plugins/streamline-icons.css" rel="stylesheet">--}}
    <link href="/css/plugins/image-picker.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/dropzone.css" rel="stylesheet">

    <!-- Event Style -->
    {{--<link href="/css/event.css" rel="stylesheet">--}}
    <link href="/css/spark/app.css" rel="stylesheet">

    {{--<link href="/css/themes/mint.css" rel="stylesheet">--}}
    <link href="/css/libs.css" rel="stylesheet">
    <link href="/css/plugins/jquery.datetimepicker.css" rel="stylesheet">
    {{--<link href="/css/bootstrap-tour.min.css" rel="stylesheet">--}}
    <link href='https://bootswatch.com/cerulean/bootstrap.min.css' rel='stylesheet' type='text/css'>

    <!--[if lt IE 9]>
    <script src="/js/ie/respond.min.js"></script>
    <![endif]-->

    <script src="/js/modernizr.min.js"></script>
    <script src="/js/plugins/pace.js"></script>
    <style>
        a{font-weight:bold;}
        body{padding-top:70px;}
    </style>

</head>

<body class="animate-page" data-spy="scroll" data-target="#navbar" data-offset="100">

@include('promo.navbar')

@yield('content')


<!-- jQuery Library -->
<script src="/js/jquery.min.js"></script>

<!-- Bootstrap JS -->
<script src="/js/bootstrap.min.js"></script>

<!-- 3rd party Plugins -->
<script src="/js/plugins/countdown.js"></script>
<script src="/js/plugins/wow.js"></script>
<script src="/js/plugins/slick.js"></script>
<script src="/js/plugins/magnific-popup.js"></script>
<script src="/js/plugins/validate.js"></script>
<script src="/js/plugins/appear.js"></script>
<script src="/js/plugins/count-to.js"></script>
<script src="/js/plugins/nicescroll.js"></script>

<!-- Main Script -->
<script type="text/javascript" src="/js/jquery.min.js"></script>
<script src="/js/libs.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.js"></script>
<script src="/js/plugins/image-picker.js"></script>
<script src="/js/plugins/jquery.datetimepicker.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('.datetimepicker').datetimepicker();
    });
</script>

@yield('footer')
@include('flash')
{{--<script>!function(e,o,n){window.HSCW=o,window.HS=n,n.beacon=n.beacon||{};var t=n.beacon;t.userConfig={},t.readyQueue=[],t.config=function(e){this.userConfig=e},t.ready=function(e){this.readyQueue.push(e)},o.config={docs:{enabled:!1,baseUrl:""},contact:{enabled:!0,formId:"4fe12d3c-62fd-11e5-8846-0e599dc12a51"}};var r=e.getElementsByTagName("script")[0],c=e.createElement("script");c.type="text/javascript",c.async=!0,c.src="https://djtflbt20bdde.cloudfront.net/",r.parentNode.insertBefore(c,r)}(document,window.HSCW||{},window.HS||{});</script>--}}
{{--<script>--}}
    {{--HS.beacon.config({--}}
        {{--color: '#dd5600',--}}
        {{--icon: 'message'--}}
    {{--})--}}
{{--</script>--}}

</body>

</html>
