@extends('promo.newlayout')
@inject('amazon', 'App\Contracts\Amazon')
<style>


</style>
    <div class="container">
        <div class="alert alert-danger" role="alert">Sorry - That offer has ended...</div>
        <h2 class="headline-support wow fadeInDown"></h2>
        <div class="page-header">
            <h1 class="text-center">But check out this other AMAZING deal on Amazon!</h1>
        </div>

        @if(! empty($relatedItems))
            @foreach ( array_slice($relatedItems, 0, 1) as $item )
                <div class="col-xs-12 col-md-6 col-md-offset-3">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{$item->Items->Item->ItemAttributes->Title}}</h3></div>
                        <div class="panel-body">
                            <a href='{{$item->Items->Item->DetailPageURL}}' target="_blank">
                                <img src='{{$amazon->returnAmazonImages($item->Items->Item->ASIN)[0]}}' class="center-block" style="width:70%;"/>
                            </a>
                        </div>
                        <div class="panel-footer">
                            <a href='{{$item->Items->Item->DetailPageURL}}' target="_blank" class="btn btn-warning btn-block btn-lg text-center">
                                Check Price on Amazon!
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
        <div class="clearfix"></div>
        <div class="col-xs-12">
            <div class="center-block text-center">
                <script type='text/javascript'>
                    amzn_assoc_ad_type = 'banner';
                    amzn_assoc_tracking_id = 'mykilkos-20';
                    amzn_assoc_marketplace = 'amazon';
                    amzn_assoc_region = 'US';
                    amzn_assoc_placement = 'assoc_banner_placement_default';
                    amzn_assoc_linkid = 'YJPBMRFRRAX37D34';
                    amzn_assoc_campaigns = 'gift_certificates';
                    amzn_assoc_banner_type = 'category';
                    amzn_assoc_isresponsive = 'true';
                </script>
                <script src='//z-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&Operation=GetScript&ID=OneJS&WS=1'></script>
            </div>


        </div>
    </div>



