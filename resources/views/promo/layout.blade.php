<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- SEO -->
    <meta name="author" content="SDR">
    <meta name="description" content="JungleCoupon - MANAGE. AMAZON. COUPONS.">
    <meta name="keywords" content="Amazon, FBA, Private Label, Amazon Coupons">

    <!-- for Facebook -->
    <meta property="og:title" content="JungleCoupon - MANAGE. AMAZON. COUPONS." />
    <meta property="og:type" content="article" />
    <meta property="og:image" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="Amazon, FBA, Private Label, Amazon Coupons"/>

    <!-- Favicons -->
    <link rel="shortcut icon" href="/images/favicon.ico">

    <!-- Page Title -->
    <title>JungleCoupon!</title>

    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Google Font : Open Sans & Montserrat -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,600' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- Plugins -->
    {{--<link href="/css/plugins/animate.css" rel="stylesheet">--}}
    {{--<link href="/css/plugins/slick.css" rel="stylesheet">--}}
    {{--<link href="/css/plugins/magnific-popup.css" rel="stylesheet">--}}
    {{--<link href="/css/plugins/font-awesome.css" rel="stylesheet">--}}
    {{--<link href="/css/plugins/streamline-icons.css" rel="stylesheet">--}}
    {{--<link href="/css/plugins/image-picker.css" rel="stylesheet">--}}

    <!-- Event Style -->

    <link href="/css/themes/red.css" rel="stylesheet">
    <link href="/css/libs.css" rel="stylesheet">
    <link href="/css/event.css" rel="stylesheet">

    <link href="/css/plugins/jquery.datetimepicker.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="/js/ie/respond.min.js"></script>
    <![endif]-->

    <script src="/js/modernizr.min.js"></script>
    <script src="/js/plugins/pace.js"></script>
@yield('header')
</head>

<body>

<div class="container">
    @yield('content')
</div>

<!--
Javascripts : Nerd Stuff :)
====================================== -->

<!-- jQuery Library -->
<script src="/js/jquery.min.js"></script>

<!-- Bootstrap JS -->
<script src="/js/bootstrap.min.js"></script>

<!-- 3rd party Plugins -->
<script src="/js/plugins/countdown.js"></script>
<script src="/js/plugins/wow.js"></script>
<script src="/js/plugins/slick.js"></script>
<script src="/js/plugins/magnific-popup.js"></script>
<script src="/js/plugins/validate.js"></script>
<script src="/js/plugins/appear.js"></script>
<script src="/js/plugins/count-to.js"></script>
<script src="/js/plugins/nicescroll.js"></script>


<!-- Main Script -->
<script src="/js/libs.js"></script>
<script src="/js/plugins/image-picker.js"></script>
<script src="/js/plugins/jquery.datetimepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#myImg").imagepicker();
        $('.datetimepicker').datetimepicker();
    });
</script>

{{--<script>--}}
    {{--$(document).ready(function(){--}}
        {{--$("#subscription option[value='{{Auth::user()->stripe_plan}}']").attr("selected","selected");--}}
    {{--});--}}
{{--</script>--}}

<script src="/js/main.js"></script>
<footer>

    {{--<div class="social-icons">--}}
    {{--<a href="#" class="wow zoomIn"> <i class="fa fa-twitter"></i> </a>--}}
    {{--<a href="#" class="wow zoomIn" data-wow-delay="0.2s"> <i class="fa fa-facebook"></i> </a>--}}
    {{--<a href="#" class="wow zoomIn" data-wow-delay="0.4s"> <i class="fa fa-linkedin"></i> </a>--}}
    {{--</div>--}}
    <p> <small class="text-muted">Copyright &copy; {{date('Y')}}. All rights reserved. JungleCoupon.com</small></p>

</footer>
@yield('footer')
@include('flash')


</body>

</html>
