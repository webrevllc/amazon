<div class="row highlight bottom-space-lg">
    <form method="post" action="/claim/{{$campaign->id}}">
        {{csrf_field()}}
        @if(count($campaign->sources) > 0 && Input::has('source'))
            <input type="hidden" name="source" value="{{\Illuminate\Support\Facades\Input::get('source')}}"/>
        @endif
        <input type="hidden" name="id" value="{{$campaign->id}}">
        <div class="col-sm-10 col-sm-offset-1">
            @if($campaign->collect_names)
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>First Name</label>
                            <input type="text" class="form-control" name="first_name" placeholder="First Name" required>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Last Name</label>
                            <input type="text" class="form-control" name="last_name" placeholder="last name" required>
                        </div>
                    </div>
                </div>
            @endif
            <div class="form-group">
                <label>Email Address</label>
                <input type="email" class="form-control" name="email" placeholder="Email Address" required>
            </div>
            <div class="text-center top-space">
                <button type="submit" class="btn btn-success btn-block btn-lg" id="js-contact-btn">Send My Coupon!</button>
                <div id="js-contact-result" data-success-msg="Form submitted successfully." data-error-msg="Oops. Something went wrong."></div>
            </div>
        </div>
    </form>
</div>