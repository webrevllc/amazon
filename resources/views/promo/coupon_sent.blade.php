@extends('promo.layout')

@section('content')

        <!-- Header Starts -->
<main id="top" class="text-center vertical-space-lg" role="main">
    <div class="container">

        <!-- Hero Title -->
        <h1 class="headline">Check Your Email! </h1>
        <!-- Sub Title -->
        <h5 class="headline-support">If you do not receive your coupon within 5 minutes check your junk mail.</h5>
        <!-- Startup Logo -->

    </div>
    @if($campaign->campaign_logo_url != '')
        <div class="col-md-6" style="padding:30px">
            <img src="{{$campaign->campaign_logo_url}}" style="width:80%" class="center-block"/>
        </div>
        <div class="col-md-6">
            <img src="{{$campaign->img_url}}" style="width:30%" class="center-block"/>
        </div>
    @else
        <br>
        <div class="logo">
            <img src="/images/check.png" alt="Check Icon">
        </div>
    <br>
    <br>
        <a href="/resend_claim/{{$claim->id}}" class="btn btn-success btn-lg"><i class="fa fa-envelope-o"></i> Resend The Email <i class="fa fa-envelope-o"></i></a>
    @endif


</main>
<div class="clearfix"></div>
<section class="pricing" id="pricing">
    <div >
        <div class="section-title">
            <h4>How Does This Work?</h4>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="pricing-item  wow zoomIn highlighted-plan" data-wow-duration="2s">
                    <div class="plan-name">Enter Your Email Address</div>
                    <div class="price"><i class="fa fa-keyboard-o"></i></div>

                    <ul class="plan-features">
                        <li>Enter your Amazon email address and you'll receive an Email Confirmation to Participate in the Promo</li>
                    </ul>
                </div>

            </div>
            <div class="col-md-4">
                <div class="pricing-item highlighted-plan wow zoomIn" data-wow-delay="0.3s" data-wow-duration="2s">
                    <div class="plan-name">Claim your Coupon</div>
                    <div class="price"><i class="fa fa-gift"> </i></div>

                    <ul class="plan-features">
                        <li> You'll land on a page containing your Coupon Code. You then Copy the Coupon Code to use on Amazon </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <div class="pricing-item wow zoomIn highlighted-plan" data-wow-delay="0.6s" data-wow-duration="2s">
                    <div class="plan-name">Buy Product on Amazon</div>
                    <div class="price"><i class="fa fa-credit-card"> </i></div>
                    <ul class="plan-features">
                        <li>You'll get the Link to purchase your product. Enter the Coupon at Checkout and Enjoy the Discount! </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- //   Header Ends -->



@stop