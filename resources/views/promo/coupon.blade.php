@extends('promo.layout')
    @section('header')
    {!! $campaign->pixel !!}
    @stop
    @section('content')
        <section class="footer-action top-space-xl">
            <div class="container">
                <h4 class="headline-support wow fadeInDown">{{$campaign->promo_title}}</h4>
                <h2 class="headline wow fadeInDown" data-wow-delay="0.1s">HERE IS YOUR COUPON</h2>
                <h2 class="headline wow fadeInDown" data-wow-delay="0.1s">{{$coupon}}</h2>
                <div class="footer_bottom-bg">
                    <a class="btn btn-success btn-xl" data-wow-delay="0.3s" href="
                    @if($campaign->super_url == '')
                        {{$amazon->productUrl($campaign->asin)}}
                    @else
                        {{$campaign->super_url}}
                    @endif
                    " ><i class="fa fa-amazon"></i>  BUY ON AMAZON NOW</a>
                </div>
            </div>
        </section>
@stop



