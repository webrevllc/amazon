<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="publishable-key" content="{{env('STRIPE_KEY')}}">
    <!-- SEO -->
    <meta name="author" content="SDR">
    <meta name="description" content="Finally a way to easily distribute your Amazon single use coupons!">
    <meta name="keywords" content="Amazon, FBA, Private Label, Amazon Coupons">

    <!-- Favicons -->
    <link rel="shortcut icon" href="/images/favicon.ico">

    <!-- Page Title -->
    <title>JungleCoupon</title>

    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Google Font : Open Sans & Montserrat -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,600' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

    <!-- Event Style -->
    <link href="/css/event.css" rel="stylesheet">

    <!-- Color Theme -->
    <!-- Options: green, purple, red, yellow, mint, blue, black  -->
    <link href="/css/themes/mint.css" rel="stylesheet">
    <link href="/css/clean-blog.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="/js/ie/respond.min.js"></script>
    <![endif]-->

    <!-- Modernizr -->
    <script src="/js/modernizr.min.js"></script>
    <!-- Subtle Loading bar -->
    <script src="/js/plugins/pace.js"></script>
    @include('tracking')
    @yield('pixel')

</head>

<body>
<!--Preloader div-->
{{--<div class="preloader"></div>--}}



@yield('content')

<!--
 Javascripts : Nerd Stuff :)
 ====================================== -->

<!-- jQuery Library -->
<script src="/js/jquery.min.js"></script>

<!-- Bootstrap JS -->
<script src="/js/bootstrap.min.js"></script>


@yield('footer')
</body>

</html>
