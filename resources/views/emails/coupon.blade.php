<html>
<head>
    <title>{{ $senderName or '' }}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <style type="text/css">{{ file_get_contents(app_path() . '/../vendor/snowfire/beautymail/src/styles/css/sunny.css') }}</style>
</head>
<body>
<table id="background-table" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
    <tr>
        <td align="center">
            <table class="w640" border="0" cellpadding="0" cellspacing="0" width="640">
                <tbody>
                <tr class="large_only">
                    <td class="w640" height="20" width="640"></td>
                </tr>
                <tr class="mobile_only">
                    <td class="w640" height="10" width="640"></td>
                </tr>
                <tr class="mobile_only">
                    <td class="w640" height="10" width="640"></td>
                </tr>
                <tr class="mobile_only">
                    <td class="w640" width="640" align="center">
                        {{--<img class="mobile_only mobile-logo" border="0" src="{{ $logo['path'] }}" alt="{{ $senderName or '' }}" width="{{ $logo['width'] }}" height="{{ $logo['height'] }}" />--}}
                    </td>
                </tr>
                <tr class="mobile_only">
                    <td class="w640" height="20" width="640"></td>
                </tr>
                <tr class="large_only">
                    <td class="w640"  height="20" width="640"></td>
                </tr>

                <tr>
                    <td class="w640" width="640" colspan="3" height="20"></td>
                </tr>

                <tr class="mobile_only">
                    <td class="w640"  height="10" width="640" bgcolor="#ffffff"></td>
                </tr>
                <tr class="mobile_only">
                    <td class="w640"  height="20" width="640" bgcolor="#ffffff"></td>
                </tr>
                <tr>
                    <td id="header" class="w640" align="center" width="640">
                        <table class="w640" border="0" cellpadding="0" cellspacing="0" width="640">
                            <tr>
                                <td class="w30" width="30"></td>
                                <td id="logo" width="300px" valign="top">
                                    @if($campaign->campaign_logo_url != '')
                                        <img border="0" src="{{$campaign->campaign_logo_url}}"  alt="{{$campaign->from_name}}" width="300" />
                                    @else
                                        <img border="0" src="https://junglecoupon.com/img/toplogo.png"  alt="JungleCoupon.com" width="300" />
                                    @endif
                                </td>
                                <td class="w30" width="30"></td>
                            </tr>
                            <tr>
                                <td colspan="3" height="20" class="large_only"></td>
                            </tr>
                            <tr>
                                <td colspan="3" height="20" class="large_only"></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td class="w640" bgcolor="#ffffff" width="640">
                        <table class="w640" border="0" cellpadding="0" cellspacing="0" width="640">
                            <tbody>
                            <tr>
                                <td class="w40" width="40"></td>
                                <td class="w560" width="560">
                                    <table class="w560" border="0" cellpadding="0" cellspacing="0" width="560">
                                        <tbody>
                                        <tr><td class="w560" height="15" width="560"></td></tr>
                                        <tr>
                                            <td class="w560" width="560">
                                                <div class="article-content" align="left">
                                                    @if($campaign->campaign_email_body != '')
                                                        {!! nl2br($campaign->campaign_email_body) !!}
                                                    @else
                                                        <p>Click the link below to claim your coupon!</p>
                                                    @endif
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td class="w40" width="40"></td>
                            </tr>
                            <tr>
                                <td colspan="3" height="30"></td>
                            </tr>
                            <tr>
                                <td class="w50" width="50"></td>
                                <td class="w560" width="560">
                                    <table class="w560" border="0" cellpadding="0" cellspacing="0" width="560">
                                        <tbody>
                                        <tr class="large_only"><td class="w560" height="15" width="560"></td></tr>
                                        <tr>
                                            <td class="w560" width="560">
                                                <div class="button-content" align="center">
                                                    <img src="{{$campaign->img_url}}" width="50%" alt="{{$campaign->promo_title}}"/>
                                                    <a href="http://junglecoupon.com/claim/{{$claim->code}}?source={{$claim->source}}" class="button">Click Here To Receive Your Promo Code</a>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td class="w50" width="50"></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="w640" bgcolor="#ffffff" width="640" colspan="3" height="20"></td>
                </tr>
                <tr>
                    <td class="w640" bgcolor="#ffffff" width="640" colspan="3" height="20"></td>
                </tr>
                <tr>
                    <td>
                        <table width="640" class="w640" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="w50" width="50"></td>
                                <td class="w410" width="410">
                                    <p id="permission-reminder" class="footer-content-left" align="left">You are receiving this email because you entered your email address to claim a coupon.</p>
                                </td>
                                <td valign="top">
                                    <table align="right">
                                        <tr>
                                            <td colspan="2" height="10"></td>
                                        </tr>
                                        <tr>
                                            @if($campaign->twitter != '')
                                                <td><a href="http://twitter.com/{{$campaign->twitter}}"><img src="https://junglecoupon.com/vendor/beautymail/assets/images/sunny/twitter.png" alt="Twitter" height="25" width="25" style="border:0" /></a></td>
                                            @endif
                                            @if($campaign->facebook != '')
                                                <td><a href="http://facebook.com/{{$campaign->facebook}}"><img src="https://junglecoupon.com/vendor/beautymail/assets/images/sunny/fb.png" alt="Facebook" height="25" width="25" style="border:0" /></a></td>
                                            @endif
                                        </tr>
                                    </table>
                                </td>
                                <td class="w15" width="15"></td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="w640" width="640" colspan="3" height="20"></td>
                </tr>
                <tr>
                    <td id="footer" class="w640" height="60" width="640" align="center">

                        &copy; {{date('Y')}} JungleCoupon.com. All rights reserved.

                    </td>
                </tr>
                <tr>
                    <td class="w640" width="640" colspan="3" height="40"></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>
