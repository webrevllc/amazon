@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.articleStart')

    <h4 class="secondary"><strong>Did you lose something?</strong></h4>
    <p>Click here to reset your password: {{ url('password/reset/'.$token) }}</p>

    <h4>No worries, we got you</h4>
    <h4>-Promo Manager</h4>
    <a href="http://junglecoupon.com">JungleCoupon.com</a>

    @include('beautymail::templates.widgets.articleEnd')
@stop

