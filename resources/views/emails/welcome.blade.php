@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.articleStart')

    <h4 class="secondary"><strong>Welcome To Jungle Coupon!</strong></h4>
    <p>Hello {{$user->name}}, and welcome to JungleCoupon.com!</p>

    <h4 class="secondary"><strong>We certainly hope you enjoy your stay</strong></h4>
    <p>If there is anything we can do to make your experience more enjoyable, please feel free to let us know!</p>
    @include('beautymail::templates.widgets.articleEnd')


    <h4>Thanks,</h4>
    <h4>Promo Team @ JungleCoupon.com</h4>

@stop

