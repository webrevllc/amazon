@extends('beautymail::templates.widgets')

@section('content')

@include('beautymail::templates.widgets.articleStart')

<h4 class="secondary"><strong>{{$campaign->campaign_name}} has launched!</strong></h4>
<h4>You can view your campaign <a href='https://junglecoupon.com/coupon/{{$campaign->promo_url}}'>here!</a></h4>

@include('beautymail::templates.widgets.articleEnd')

<h4>Best of luck,</h4>
<h4>Promo Team @ JungleCoupon.com</h4>

@stop
