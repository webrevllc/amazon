@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.articleStart')

    <h4 class="secondary"><strong>Hey, a coupon was claimed for {{$campaign->campaign_name}}</strong></h4>
    <p>Watch Your Amazon Seller Account and see if {{$email}} purchases from you!</p>

    @include('beautymail::templates.widgets.articleEnd')

    <h4>-Promo Manager</h4>
    <a href="http://junglecoupon.com">JungleCoupon.com</a>

@stop

