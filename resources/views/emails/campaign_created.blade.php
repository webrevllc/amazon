@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.articleStart')

    <h4 class="secondary"><strong>Congrats on the new campaign, {{$campaign->campaign_name}}!</strong></h4>
    <h4>We want to make sure this campaign is as successful as possible.</h4>
    <h4>Let us know if there is anything we can do to assist.</h4>

    <h3>Don't Forget:</h3>
    <h4>Make sure to upload your coupons in the coupon management area.</h4>
    {{--<h4>Also, you can synchronize your campaign with one of your MailChimp lists to market to your shoppers later!</h4>--}}

    @include('beautymail::templates.widgets.articleEnd')

    <h4>Best of luck,</h4>
    <h4>Promo Team @ JungleCoupon.com</h4>

@stop
