@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.articleStart')

    <h4 class="secondary"><strong>New User Signed Up!</strong></h4>
    <p>{{$user->stripe_plan}}</p>
    <p>{{$user->name}}</p>
    <p>{{$user->email}}</p>

    <h4>Congrats!,</h4>

    @include('beautymail::templates.widgets.articleEnd')

    <h4>Best of luck,</h4>
    <h4>Promo Team @ JungleCoupon.com</h4>

@stop

