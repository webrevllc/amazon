<h1>New User Signed Up!</h1>

{{$user->name}}
{{$user->email}}


@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.articleStart')

    <h4 class="secondary"><strong>New User Signed Up!</strong></h4>
    <p>{{$user->stripe_plan}}</p>
    <p>{{$user->name}}</p>
    <p>{{$user->email}}</p>

    <h4>Congrats!,</h4>

@stop

