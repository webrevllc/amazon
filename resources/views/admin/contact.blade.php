@extends('admin.layout')

@section('content')
    <div class="container" id="contact">
        <div class="section-title">
            <h5>HAVE QUESTIONS??? ASK AWAY!</h5>
            <p>If you have any questions about JungleCoupon, please contact us directly. We will respond as quickly as possible.</p>
        </div>

        <div class="contact-form bottom-space-xl wow fadeInUp">

            <form action="/message" method="POST">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" class="form-control" name="first_name" placeholder="First Name" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" class="form-control" name="last_name" placeholder="last name" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Email Address</label>
                            <input type="email" class="form-control" name="email" placeholder="Email Address" required>
                        </div>
                        <div class="form-group">
                            <label>Phone Number</label>
                            <input type="text" class="form-control" name="phone" placeholder="Optional">
                        </div>
                        <div class="form-group">
                            <label>Your Message</label>
                            <textarea class="form-control" name="message" rows="6" placeholder="Enter your message here" required> </textarea>
                        </div>

                        <div class="text-center top-space">
                            <button type="submit" class="btn btn-success btn-block btn-lg" id="js-contact-btn">Send Message</button>
                            <div id="js-contact-result" data-success-msg="Form submitted successfully." data-error-msg="Oops. Something went wrong."></div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>

@stop
