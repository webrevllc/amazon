@extends('admin.layout')
<style>
    .btn-group{
        top: 20px;
    }
</style>
@section('content')
    <header id="top" class="sub-header">
        <div class="container">
            <h3 class="page-title wow fadeInDown">Admin</h3>
        </div>
        <!-- end .container -->
    </header>


    <div class="container top-space-xl">

        <ul class="nav nav-tabs" role="tablist">
            <li class="active"><a href="#">Users</a></li>
            <li><a href="/admin/admin/reports">Reports</a></li>
        </ul>

        @include('admin.admin.partials.users_table')
    </div>





@stop




