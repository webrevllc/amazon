<table class="table table-bordered table-hover" id="step1">
    <thead>
    <tr>
        <th>Name</th>
        <th>Email Address</th>
        <th>Member Since</th>
        <th>Active</th>
        <th>Stripe Plan</th>
        <th>Trial End Date</th>
    </tr>
    </thead>
    <tbody>
    @foreach(allUsers() as $user)
        <tr>
            <td><a href="/secretsignin/{{$user->id}}">{{$user->name}}</a></td>
            <td>{{$user->email}}</td>
            <td>{{date('F j, Y', strtotime($user->created_at))}}</td>
            <td>{{$user->stripe_active}}</td>
            <td>{{$user->stripe_plan}}</td>
            <td>{{date('F j, Y', strtotime($user->trial_ends_at))}}</td>
        </tr>
    @endforeach
    </tbody>
</table>