@extends('admin.layout')
<style>
    .btn-group{
        top: 20px;
    }
</style>
@section('content')
    <header id="top" class="sub-header">
        <div class="container">
            <h3 class="page-title wow fadeInDown">Admin</h3>
        </div>
        <!-- end .container -->
    </header>


    <div class="container top-space-xl">

        <ul class="nav nav-tabs" role="tablist">
            <li><a href="/admin/admin">Users</a></li>
            <li class="active"><a href="#">Reports</a></li>
        </ul>

        <h4>Total Users: {{count(allUsers())}}</h4>
        <h4>Monthly Subscribers: {{userBySubscription('7dayJM')}}</h4>
        <h4>Quarterly Subscribers: {{userBySubscription('7dayJQ')}}</h4>
        <h4>Yearly Subscribers: {{userBySubscription('7dayJY')}}</h4>
        <br>
        <h4>Campaigns Created Today: {{campaignsCreatedToday()}}</h4>
        <h4>Total Live Campaigns: {{totalLiveCampaigns()}}</h4>
        <h4>Coupons Claimed Today: {{couponsClaimedToday()}}</h4>



    </div>





@stop




