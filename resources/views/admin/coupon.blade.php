@extends('admin.layout')
<style>
    .btn-group{
        top: 35px;
    }
</style>
@section('content')
    <header id="top" class="sub-header">
        <div class="container">
            <h3 class="pull-left page-title wow fadeInDown">Coupon Management</h3>
            <div class="pull-right">
                <div class="btn-group">
                    <button class="btn btn-default " data-toggle="modal" data-target="#myModal">
                        <i class="fa fa-plus-circle"></i> Add Coupons
                    </button>
                    <a class="btn btn-default " href="/admin/campaigns/{{$campaign->id}}">
                        <i class="fa fa-eye"></i> View Campaign
                    </a>
                    <a class="btn btn-default " href="/admin/campaigns/{{$campaign->id}}/edit">
                        <i class="fa fa-bar-chart"></i> Manage Campaign
                    </a>

                </div>
            </div>
            <div class="clearfix"></div>
            <h5>Total Coupons: {{count($campaign->coupons)}}</h5>
            <h5>Coupons Used: {{$campaign->claimed}}</h5>
        </div>
        <!-- end .container -->
    </header>
    <div class="container top-space-lg">
        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>Coupon</th>
                <th>Email Address of Customer</th>
            </tr>
            </thead>
            <tbody>
            @foreach($campaign->coupons as $coupon)
                <tr>
                    <td>{{$coupon->coupon}}</td>
                    <td>{{$coupon->assigned_to}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>



    <div class="modal fade" id="mcModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Which List Would You like To Send Customers To?</h4>
                </div>
                <div class="modal-body">
                    <form method="POST" action="/admin/selectedList" id="api">
                        {{csrf_field()}}
                        <select id="lists" class="form-control" name="lists"></select>
                        <input type="hidden" value="{{$campaign->id}}" name="campaignId"/>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger" data-dismiss="modal" onclick="submitList()">Submit</button>
                </div>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload More Coupons</h4>
                </div>
                <div class="modal-body">
                    <form method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label>Add Coupons:</label>
                            <input name="coupons" type="file" >
                        </div>
                        <button type="submit" class="btn btn-danger"><i class="fa fa-cloud-upload"></i> Upload</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var getMCLists = function(){
            $.getJSON('/admin/get_mc_lists', function(data) {
                var html = '';
                var len = data.length;
                for (var i = 0; i< len; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
                }
                $('#lists').append(html);
            });
        };

        var submitList = function(){
            var form = $('#api');
            form.submit();
        };
    </script>
@stop


