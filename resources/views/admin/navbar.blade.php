<nav class="navbar navbar-inverse navbar-fixed-top ">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><img src="/img/new-junglecoupon.png" alt="Gather"> </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">

            <ul class="nav navbar-nav navbar-right">
                <li class="{{ set_active(['admin/']) }}"><a href="/admin">Home</a></li>
                <li class="{{ set_active(['admin/campaigns','admin/campaigns*']) }}"><a href="/admin/campaigns">Campaigns</a></li>
                {{--<li class="{{ set_active(['admin/questions']) }}"><a href="#questions">Questions</a></li>--}}
                <li class="dropdown {{ set_active(['/profile']) }}">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My Account <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/admin/profile/account">Profile</a></li>
                        <li><a href="/admin/faq">FAQ's</a></li>
                        <li><a href="/admin/contact-us">Contact Us</a></li>
                        @can('view_admin')
                        <li><a href="/admin/admin">Admin</a></li>
                        @endcan
                        <li role="separator" class="divider"></li>
                        <li><a href="/logout">Sign Out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
</nav>
