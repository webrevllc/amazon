@extends('admin.layout')
@inject('C', 'App\Campaign')

@section('content')
    <div class="container spark-screen">
        <div class="row">
            <!-- Tabs -->
            <div class="col-md-4">
                <div class="panel panel-info panel-flush">
                    <div class="panel-heading">
                        Settings
                    </div>
                    <div class="panel-body">
                        <div class="spark-settings-tabs">
                            @include('admin.campaigns.partials.navbar')
                        </div>
                    </div>
                </div>
            </div>

            <!-- Tab Panes -->
            <div class="col-md-8">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        {{$campaign->campaign_name}}
                    </div>
                    <div class="panel-body">
                        <button class="btn btn-warning btn-lg btn-block" data-toggle="modal" data-target="#myModal">
                            <i class="fa fa-plus-circle"></i> Add Coupons
                        </button>

                        <br>
                        <br>
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Delete?</th>
                                <th>Coupon</th>
                                <th>Customer Email</th>
                                {{--<th>Customer Name</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($campaign->coupons as $coupon)
                                <tr>
                                    <td class="col-xs-1">
                                        <a href="/admin/delete-coupon/?id={{$coupon->id}}" class="btn btn-danger btn-md "><i class="fa fa-trash-o"></i> </a>
                                    </td>
                                    <td>{{$coupon->coupon}}</td>
                                    <td>{{$coupon->assigned_to}}</td>
                                    {{--<td>{{$coupon->first_name. ' '. $coupon->last_name}}</td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @if(count($campaign->coupons))
                        <a href="/admin/delete-coupons/?id={{$campaign->id}}" class="btn btn-danger btn-block btn-lg" >
                            <i class="fa fa-trash-o"></i> DELETE ALL COUPONS
                        </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload More Coupons</h4>
                </div>
                <div class="modal-body">
                    <form method="POST" enctype="multipart/form-data" action="/admin/coupons/{{$campaign->id}}">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label>Add Coupons:</label>
                            <input name="coupons" type="file" >
                        </div>
                        <button type="submit" class="btn btn-danger"><i class="fa fa-cloud-upload"></i> Upload</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


@stop

