@extends('admin.layout')

@section('content')
    <style>
        ul.thumbnails.image_picker_selector li{width:20%;}
    </style>
    <div class="container spark-screen">
        <div class="row">
            <!-- Tabs -->
            <div class="col-md-4">
                <div class="panel panel-info panel-flush">
                    <div class="panel-heading">
                        Settings
                    </div>
                    <div class="panel-body">
                        <div class="spark-settings-tabs">
                            @include('admin.campaigns.partials.navbar')
                        </div>
                    </div>
                </div>
            </div>

            <!-- Tab Panes -->
            <div class="col-md-8">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        {{$campaign->campaign_name}}
                    </div>
                    <div class="panel-body">
                        @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $err)
                                        <li>{{$err}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if($images == [])
                            <h3 class="text-center">Sorry, that ASIN returned no featured images. Make sure that product has images then refresh this page.</h3>
                        @else
                            <h6 >Even if you plan to use a video on the landing page, select an image to display on outgoing emails.</h6>
                                <form method="post" action="/admin/campaigns/{{$campaign->id}}/featured_img">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label>Select Featured Image:</label><br>
                                        <select id="myImg" name="img_url" class="image-picker">
                                            @foreach($images as $imageUrl)
                                                <option data-img-src="{{$imageUrl}}" value="{{$imageUrl}}"></option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div>
                                        <button type="submit" class="btn btn-warning pull-right btn-lg ">Update Featured Image</button>
                                    </div>
                                </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('footer')

    <script type="text/javascript">
        $(document).ready(function () {
            $("#myImg").imagepicker();
        });
    </script>
@stop
