@extends('admin.layout')
<style>
    .btn-group{
        top: 20px;
    }
</style>
@section('content')
    <header id="top" class="sub-header">
        <div class="container">
            <div class="page-header">
                <h1>All of your campaigns
                    <small><a href="/admin/campaigns/create" class="btn btn-warning btn-xs "><i class="fa fa-plus"></i> Create a Campaign</a></small></h1>
            </div>
        </div>
        <!-- end .container -->
    </header>
    <div class="container top-space-xl">
        @include('admin.campaigns.partials.tabs')
        @include('admin.campaigns.partials.trashed_campaign_table')
    </div>
@stop




