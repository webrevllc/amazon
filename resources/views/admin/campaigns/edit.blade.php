@extends('admin.layout')
@inject('C', 'App\Campaign')
<style>

    ul.thumbnails.image_picker_selector li{width:20%;}
    .btn-group{
        top: 20px;
    }


    input[type=checkbox] {
        display: none;
    }
    /* to hide the checkbox itself */

    input[type=checkbox] + label:before {
        font-family: FontAwesome;
        display: inline-block;
    }

    input[type=checkbox] + label:before {
        content: "\f096";
    }
    /* unchecked icon */

    input[type=checkbox] + label:before {
        letter-spacing: 10px;
    }
    /* space between checkbox and label */

    input[type=checkbox]:checked + label:before {
        content: "\f046";
    }
    /* checked icon */

    input[type=checkbox]:checked + label:before {
        letter-spacing: 5px;
    }
    /* allow space for check mark */
</style>
@section('content')
    <div class="container spark-screen">
        <div class="row">
            <!-- Tabs -->
            <div class="col-md-4">
                <div class="panel panel-info panel-flush">
                    <div class="panel-heading">
                        Settings
                    </div>
                    <div class="panel-body">
                        <div class="spark-settings-tabs">
                            @include('admin.campaigns.partials.navbar')
                        </div>
                    </div>
                </div>
            </div>
            <!-- Tab Panes -->
            <div class="col-md-8">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        {{$campaign->campaign_name}}
                    </div>
                    <div class="panel-body">
                        @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $err)
                                        <li>{{$err}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {!! Form::model($campaign, array('route' => array('admin.campaigns.update', $campaign->id), 'method' => 'PUT' )) !!}
                        <div class="form-group">
                            <label>Campaign Name:</label>
                            <input class="form-control" name="campaign_name" type="text" value="{{$campaign->campaign_name}}" id="step1">
                            <p class="help-block">Example: Kitty Scratcher $10 off</p>
                        </div>
                        <div class="form-group">
                            <label for="promo_title">Promo Title:</label>
                            <input class="form-control" name="promo_title" id="promo_title" type="text" value="{{$campaign->promo_title}}" >
                            <p class="help-block">Appears at the top of the coupon landing page</p>
                        </div>
                        <div class="form-group">
                            <input id="reviews"  name="reviews" type="checkbox" <?php $r = $campaign->reviews ?  'checked' :  '' ; echo $r?>/>
                            <label for="reviews" id="reviewsl">Show Reviews From Amazon?</label>
                        </div>
                        <div class="form-group">
                            <input id="filter_ips" name="filter_ips" type="checkbox" <?php $c = $campaign->filter_ips ?  'checked' :  '' ; echo $c?> />
                            <label for="filter_ips" id="filterips">1 Coupon Per IP Address?</label>
                        </div>
                        <div class="form-group">
                            <input id="filter_emails" name="filter_emails" type="checkbox" <?php $filter_emails = $campaign->filter_emails ?  'checked' :  '' ; echo $filter_emails?>/>
                            <label for="filter_emails" id="filterdisps">Filter Known Disposable Email Addresses</label>
                        </div>
                        <div class="form-group">
                            <input id="use_video" name="use_video" type="checkbox" <?php $use_video = $campaign->use_video ?  'checked' :  '' ; echo $use_video?>/>
                            <label for="use_video" id="use_video">Show YouTube Video on Landing Page</label>
                        </div>
                        <div class="form-group">
                            <input id="show_coupons" name="show_coupons" type="checkbox" <?php $show_coupons = $campaign->show_coupons ?  'checked' :  '' ; echo $show_coupons?>/>
                            <label for="show_coupons" id="show_coupons">Show Coupons Remaining Section</label>
                        </div>
                        <div class="form-group">
                            <input id="collect_names" name="collect_names" type="checkbox" <?php $collect_names = $campaign->collect_names ?  'checked' :  '' ; echo $collect_names?>/>
                            <label for="collect_names" id="collect_names">Collect First and Last Names?</label>
                        </div>

                        <div class="form-group">
                            <label for="regular_price">Regular Price:</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-dollar"></i></div>
                                <input class="form-control"  name="regular_price" id="regular_price" type="text" value="{{$campaign->regular_price}}" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="discount_price">Promo Price:</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-dollar"></i></div>
                                <input class="form-control" name="discount_price" id="discount_price" type="text" value="{{$campaign->discount_price}}" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="datepicker">Promotion Ends:</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <input class="form-control datetimepicker" id="begins" type="text" name="end_date" value="{{date('Y-m-d H:i:s', strtotime($campaign->end_date) )}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="url">Super URL:</label>
                            <div class="input-group">
                                <div class="input-group-addon yellow"><i class="fa fa-star"></i></div>
                                <input class="form-control" id="url" type="text" name="super_url" value="{{$campaign->super_url}}">
                            </div>
                            <p class="help-block">Whatever you paste in here will be the link your customers will click on the coupon redemption page. Make sure you include 'https://'. If you want to just use the default URL leave this field blank.</p>
                        </div>
                        <button type="submit" class="btn btn-warning btn-lg pull-right">Update Campaign</button>
                        {!!  FORM::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .yellow{background:#dd5600;color:#fff;}
    </style>
@stop

