@extends('admin.layout')

@section('content')
    <div class="container spark-screen">
        <div class="row">
            <!-- Tabs -->
            <div class="col-md-4">
                <div class="panel panel-info panel-flush">
                    <div class="panel-heading">
                        Settings
                    </div>
                    <div class="panel-body">
                        <div class="spark-settings-tabs">
                            @include('admin.campaigns.partials.navbar')
                        </div>
                    </div>
                </div>
            </div>

            <!-- Tab Panes -->
            <div class="col-md-8">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        {{$campaign->campaign_name}}
                    </div>
                    <div class="panel-body">
                        @if(Session::has('success'))
                            <div class="alert alert-success" role="alert">
                                <h4>{{ Session::get('success') }}</h4>
                            </div>
                        @endif
                        @if(Session::has('error'))
                            <div class="alert alert-danger" role="alert">
                                <h4>{{ Session::get('error') }}</h4>
                            </div>
                        @endif
                            <div class="alert alert-info">
                                <h4 style="margin:0px;">New! When your campaign is paused or coupons are all claimed or timer has run down to 0, now you can control how your landing page looks!</h4>
                            </div>
                        <form action="/admin/campaigns/{{$campaign->id}}/email" method="post">
                            {{csrf_field()}}

                            <div class="form-group" id="radios">
                                <div class="radio">
                                    <label for="custom">
                                        <input type="radio" name="display" id="custom" value="custom" checked>
                                        <h4 style="margin:0px;">I want to show a related product that I choose.</h4>
                                    </label>
                                </div>
                                <div class="form-group" id="asin" >
                                    <label for="featured_asin">Enter the ASIN for the product you would like featured.</label>
                                    <input class="form-control" id="featured_asin" name="featured_asin" type="text" value="{{$campaign->featured_asin}}" required>
                                </div>
                                <br>
                                <div class="radio">
                                    <label for="redirect">
                                        <input type="radio" name="display" value="redirect" id="redirect">
                                        <h4 style="margin:0px;">I want to redirect to a custom url.</h4>
                                    </label>
                                </div>
                                <div class="form-group" id="url" >
                                    <label for="asin">Enter any URL you would like your customers redirected to</label>
                                    <input class="form-control" id="asin" name="email_name" type="text" value="{{$campaign->featured_redirect}}" required>
                                </div>
                                <br>
                                <div class="radio disabled">
                                    <label for="collect">
                                        <input type="radio" name="display" id="collect" value="collect">
                                        <h4 style="margin:0px;">I want to collect email addresses of people that get to this page.</h4>
                                    </label>
                                </div>
                                <div class="form-group" id="list" >
                                    <label for="asin">Custom URL:</label>
                                    <select name="mc_list" class="form-control">
                                        @if($campaign->mc_api_key != '')
                                            @if($mclists != [])
                                                @foreach($mclists as $list)
                                                    <option value="{{$list['id']}}">{{$list['name']}}</option>
                                                @endforeach
                                            @else
                                                <option>Invalid API Key</option>
                                            @endif
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-lg pull-right btn-warning">Update Campaign</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>


@stop

@section('footer')

@stop