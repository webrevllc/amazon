@extends('promo.layout')
@section('header')
    <style>
        ul.arrow {
            list-style-image: url('https://stockadetrees.files.wordpress.com/2014/07/red-check.jpg?w=39&h=34');
        }
        ul.arrow li{margin-bottom:25px;}
    </style>
@stop
@if($campaign->status == '' || $campaign->status == 'READY TO LAUNCH')
    <span class="alert alert-warning" style="position: fixed;z-index: 999;">Campaign Has Not Launched Yet<br>
        <a href="/admin/campaigns/{{$campaign->id}}/edit">Edit</a> |
        <a href="/admin/campaigns/{{$campaign->id}}/launch">LAUNCH</a>
    </span>

@elseif($campaign->status == 'LAUNCHED')
    <span class="alert alert-success" style="position: fixed;z-index: 999;">CAMPAIGN IS LIVE<br> <a href="/admin/campaigns/{{$campaign->id}}/edit">Edit</a></span>
@endif

@section('content')

    <style>
        .stats-info .fa, .stats-info p, .discount-price{color: red;}
    </style>
    <div class="container">
        <div class="page-header">
            @if($campaign->campaign_logo_url != '')
                <img src="{{$campaign->campaign_logo_url}}" style="width:30%" class="center-block"/>
            @else
                <img src="/img/amazon.jpg" style="width:30%" class="center-block"/>
            @endif
            <section class="">
                <div class="countdown_wrap">
                    <div class="col-md-6">
                        <h3 class="text-center coupon-title">
                            {{$campaign->promo_title}}
                            <div class="text-center">
                                <h5>Regular Price: {{$campaign->regular_price}}</h5>
                                <h3 class="discount-price">Your Price: ${{$campaign->discount_price}}</h3>
                            </div>
                        </h3>
                    </div>
                    <div class="col-md-6">
                        <h6 class="countdown_title text-center">Time's Running Out - Act Quickly</h6>
                        <ul id="countdown" data-event-date="{{date('d F Y H:i:s', strtotime($campaign->end_date))}}">
                            <li class="wow zoomIn" data-wow-delay="0s" style="visibility: visible; animation-delay: 0s; -webkit-animation-delay: 0s; animation-name: zoomIn; -webkit-animation-name: zoomIn;"> <span class="days">406</span>
                                <p class="timeRefDays">days</p>
                            </li>
                            <li class="wow zoomIn" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; -webkit-animation-delay: 0.2s; animation-name: zoomIn; -webkit-animation-name: zoomIn;"> <span class="hours">12</span>
                                <p class="timeRefHours">hours</p>
                            </li>
                            <li class="wow zoomIn" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; -webkit-animation-delay: 0.4s; animation-name: zoomIn; -webkit-animation-name: zoomIn;"> <span class="minutes">29</span>
                                <p class="timeRefMinutes">minutes</p>
                            </li>
                            <li class="wow zoomIn" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; -webkit-animation-delay: 0.6s; animation-name: zoomIn; -webkit-animation-name: zoomIn;"> <span class="seconds">39</span>
                                <p class="timeRefSeconds">seconds</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </section>
        </div>
        <div class="clearfix"></div>
        <div class="row highlight bottom-space-lg">
            <form>
                <div class="form-group col-md-3 hidden-sm">
                    <h6 class="susbcribe-head wow fadeInLeft"> CLAIM <small>Your Coupon Now</small></h6>
                </div>
                <div class="form-group col-sm-8 col-md-6 wow fadeInRight">
                    <label class="sr-only">Email address</label>
                    <input type="email" class="form-control input-lg" placeholder="Enter your email" name="email" id="email" required disabled>
                </div>
                <div class="form-group col-sm-4 col-md-3">
                    <button type="submit" class="btn btn-lg btn-success btn-block" id="js-subscribe-btn">Send My Coupon</button>
                </div>
            </form>
        </div>
        <div class="row">
            <div class="col-md-7">
                <ul class="fa-ul">
                    @foreach($amazon->returnFeatures($campaign->asin) as $feature)
                        <li><i class="fa-li fa fa-2x fa-magic" style="color:red;"></i><h5>{{$feature}}</h5></li>
                        <li></li>
                    @endforeach
                </ul>

            </div>
            <div class="col-md-5">
                @if($campaign->use_video)
                    <iframe width="100%" height="300px" src="https://www.youtube.com/embed/{{$campaign->videoID}}" frameborder="0" allowfullscreen></iframe>
                @else
                    <img src="{{$campaign->img_url}}" width="100%;"/>
                @endif
            </div>
        </div>

        @if($campaign->show_coupons)
            <section class="highlight our-stats" id="stats-counter">
            <div class="row">
                <div class="col-sm-3">
                    <div class="stats-info">
                        <i class="fa fa-microphone"></i>
                        <h2><span class="count" data-from="0" data-to="{{count($campaign->coupons)}}" data-speed="1000">{{count($campaign->coupons)}}</span></h2>
                        <p>Total Coupons Made</p>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="stats-info">
                        <i class="fa fa-minus"></i>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="stats-info">
                        <i class="fa fa-user"></i>
                        <h2><span class="count" data-from="0" data-to="{{$campaign->claimed}}" data-speed="1000">{{$campaign->claimed}}</span></h2>
                        <p>Claimed</p>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="stats-info">
                        <i class="fa fa-ticket"></i>
                        <h2><span class="count" data-from="0" data-to="{{count($campaign->coupons) - $campaign->claimed}}" data-speed="1000">{{count($campaign->coupons) - $campaign->claimed}}</span></h2>
                        <p>Remaining to be claimed!</p>
                    </div>
                </div>
            </div>
            <!-- end .container -->

        </section>
        @endif
        <!-- end section.our-stats -->
        @if($campaign->reviews)
            <section class="top-space-xl">
                <iframe src="{{preg_replace("/^http:/i", "https:", $amazon->reviews($campaign->asin))}}" width="100%" height="1100px" frameborder="0"></iframe>
            </section>
        @endif
        <div class="row highlight top-space-xl">
            <form>
                <div class="form-group col-md-3 hidden-sm">
                    <h6 class="susbcribe-head wow fadeInLeft"> CLAIM <small>Your Coupon Now</small></h6>
                </div>
                <div class="form-group col-sm-8 col-md-6 wow fadeInRight">
                    <label class="sr-only">Email address</label>
                    <input type="email" class="form-control input-lg" placeholder="Enter your email" name="email" id="email" required disabled>
                </div>
                <div class="form-group col-sm-4 col-md-3">
                    <button type="submit" class="btn btn-lg btn-success btn-block" id="js-subscribe-btn">Send My Coupon</button>
                </div>
            </form>
        </div>
        <section class="pricing" id="pricing">
            <div >
                <div class="section-title">
                    <h4>How Does This Work?</h4>
                    <p>Enter your email address above and we will send you a coupon.</p>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="pricing-item  wow zoomIn highlighted-plan" data-wow-duration="2s">
                            <div class="plan-name">Enter Your Email Address</div>
                            <div class="price"><i class="fa fa-keyboard-o"></i></div>

                            <ul class="plan-features">
                                <li>Enter your Amazon email address and you'll receive an Email Confirmation to Participate in the Promo</li>
                            </ul>
                        </div>

                    </div>
                    <div class="col-md-4">
                        <div class="pricing-item highlighted-plan wow zoomIn" data-wow-delay="0.3s" data-wow-duration="2s">
                            <div class="plan-name">Claim your Coupon</div>
                            <div class="price"><i class="fa fa-gift"> </i></div>

                            <ul class="plan-features">
                                <li> You'll land on a page containing your Coupon Code. You then Copy the Coupon Code to use on Amazon </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="pricing-item wow zoomIn highlighted-plan" data-wow-delay="0.6s" data-wow-duration="2s">
                            <div class="plan-name">Buy Product on Amazon</div>
                            <div class="price"><i class="fa fa-credit-card"> </i></div>
                            <ul class="plan-features">
                                <li>You'll get the Link to purchase your product. Enter the Coupon at Checkout and Enjoy the Discount! </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
