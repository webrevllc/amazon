@extends('admin.layout')
<style>

</style>
@section('content')
    <header id="top" class="sub-header">
        <div class="container">
            <h3 class="page-title wow fadeInDown">Create a new campaign!</h3>
        </div>
    </header>
    <div class="container top-space-lg">
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $err)
                        <li>{{$err}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form role="form" method="post" action="/admin/campaigns">
            {{csrf_field()}}
            <div class="form-group">
                <label>Campaign Name</label>
                <input class="form-control" name="campaign_name" type="text" id="campaign_name" required value="{{old('campaign_name')}}">
                <p class="help-block">This can be any name, so you can identify it later: Kitty Scratcher $10 off</p>
            </div>
            <div class="form-group">
                <label for="asin">Amazon Product ASIN:</label>
                <input class="form-control" name="asin" type="text" id="asin" required value="{{old('asin')}}">
                <p class="help-block">Hint: If you can't find it <a href="http://askville.amazon.com/find-Amazon-ASIN-product-details-page/AnswerViewer.do?requestId=11106037" target="_blank">Click here.</a></p>
            </div>
            <div class="form-group">
                <label for="promo_url">Promo Url:</label>
                <div class="input-group">
                    <div class="input-group-addon">https://junglecoupon.com/coupon/</div>
                    <input id="step3"
                       data-toggle="tooltip"
                       title="Your promo url is the link you will drive visitors to, it will be www.junglcoupon.com/coupon/whatever-you-type-in-here, may NOT contain spaces."
                       class="form-control "
                       name="promo_url"
                       type="text"
                       required
                       value="{{old('promo_url')}}"
                       >
                    </div>
            </div>
            <div class="form-group">
                <label for="datepicker2">Promotion Ends:</label>
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                    <input class="form-control datetimepicker" id="datepicke2r" type="text" name="end_date" required value="{{old('end_date')}}">
                </div>
            </div>
            <div class="form-group">
                <div>
                    <button type="submit" class="btn btn-warning btn-lg pull-right" id="create">Start Building</button>
                </div>
            </div>
        </form>
    </div>
@stop

