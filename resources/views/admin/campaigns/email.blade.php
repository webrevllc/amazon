@extends('admin.layout')

@section('content')
    <div class="container spark-screen">
        <div class="row">
            <!-- Tabs -->
            <div class="col-md-4">
                <div class="panel panel-info panel-flush">
                    <div class="panel-heading">
                        Settings
                    </div>
                    <div class="panel-body">
                        <div class="spark-settings-tabs">
                            @include('admin.campaigns.partials.navbar')
                        </div>
                    </div>
                </div>
            </div>

            <!-- Tab Panes -->
            <div class="col-md-8">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        {{$campaign->campaign_name}}
                    </div>
                    <div class="panel-body">
                        @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $err)
                                        <li>{{$err}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="/admin/campaigns/{{$campaign->id}}/email" method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="email_name">Email 'From' Name:</label>
                                <input class="form-control" id="email_name" name="email_name" type="text" value="{{$campaign->email_name}}" required>
                                <p class="help-block">Example: ABC Store Inc.</p>
                            </div>
                            <div class="form-group">
                                <label for="email_address">Email 'From' Address:</label>
                                <input class="form-control" id="email_address" name="email_address" type="text" value="{{$campaign->email_address}}" required>
                                <p class="help-block">Example: john@abcstoreinc.com</p>
                            </div>
                            <div class="form-group">
                                <label for="email_subject">Email Subject:</label>
                                <input class="form-control" id="email_subject" name="email_subject" type="text" value="{{$campaign->email_subject}}" required>
                                <p class="help-block">Example: ABC Stores Coupon - Just For You!</p>
                            </div>
                            <div class="form-group">
                                <label for="facebook">Facebook:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">https://facebook.com/</div>
                                    <input type="text" class="form-control" name="facebook" id="facebook" value="{{$campaign->facebook}}" placeholder="abc-store-inc">
                                </div>
                                <p class="help-block">Leave blank if none</p>
                            </div>
                            <div class="form-group">
                                <label for="twitter">Twitter:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">https://twitter.com/</div>
                                    <input type="text" class="form-control" name="twitter" id="twitter" value="{{$campaign->twitter}}" placeholder="abc-store-inc">
                                </div>
                                <p class="help-block">Leave blank if none</p>
                            </div>
                            <div class="form-group">
                                <label for="email_body">Email body:</label>
                                <textarea rows="10" id="email_body" class="form-control" name="campaign_email_body" required>{!! $campaign->campaign_email_body !!}</textarea>
                            </div>
                            <button type="submit" class="btn btn-lg pull-right btn-warning">Update Campaign</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
