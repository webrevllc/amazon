@extends('admin.layout')

@section('content')
    <div class="container spark-screen">
        <div class="row">
            <!-- Tabs -->
            <div class="col-md-4">
                <div class="panel panel-info panel-flush">
                    <div class="panel-heading">
                        Settings
                    </div>
                    <div class="panel-body">
                        <div class="spark-settings-tabs">
                            @include('admin.campaigns.partials.navbar')
                        </div>
                    </div>
                </div>
            </div>

            <!-- Tab Panes -->
            <div class="col-md-8">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        {{$campaign->campaign_name}}
                    </div>
                    <div class="panel-body">
                        <form method="post" action="/admin/campaigns/{{$campaign->id}}/video">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="video">Enter The YouTube Video ID:</label><br>
                                <div class="input-group">
                                    <div class="input-group-addon">https://www.youtube.com/watch?v=</div>
                                    <input id="video" name="videoID" type="text" class="form-control" value="{{$campaign->videoID}}"/>
                                </div>
                                <p class="help">Remember: To toggle between using video or a featured image on the landing page visit that option in the General Information tab.</p>
                            </div>
                            <div class="clearfix"></div>
                            <br>
                            @if($campaign->videoID != '')
                                <iframe width="100%" height="400px" src="https://www.youtube.com/embed/{{$campaign->videoID}}" frameborder="0" allowfullscreen></iframe>
                            @endif
                            <div class="text-center top-space bottom-space-xl">
                                <br>
                                <button type="submit" class="btn btn-warning pull-right btn-lg ">Use This Video</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

