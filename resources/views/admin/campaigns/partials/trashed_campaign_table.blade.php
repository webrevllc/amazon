<table class="table table-bordered table-hover" id="step1">
    <thead>
    <tr>
        <th>Restore?</th>
        <th>Name</th>
        <th>Created Date</th>
        <th>Deleted Date</th>
    </tr>
    </thead>
    <tbody>
    @foreach($trashed as $campaign)
    <tr>
        <td class="col-md-1"><a class="btn btn-sm btn-success btn-block" href="/admin/campaigns/{{$campaign->id}}/restore"><i class="fa fa-refresh text-center"></i></a></td>
        <td>{{$campaign->campaign_name}}</td>
        <td>{{date('F j, Y', strtotime($campaign->created_at))}}</td>
        <td>{{date('F j, Y', strtotime($campaign->deleted_at))}}</td>
    </tr>
    @endforeach
    </tbody>
</table>