<ul class="nav spark-settings-tabs-stacked" role="tablist">
    <li role="presentation" class="{{Request::is('admin/campaigns/*/edit') ? 'active' : ''}}"><a href="/admin/campaigns/{{$campaign->id}}/edit">General Information</a></li>
    <li role="presentation" class="{{Request::is('admin/campaigns/*/email') ? 'active' : ''}}"><a href="/admin/campaigns/{{$campaign->id}}/email">Email</a></li>
    <li role="presentation" class="{{Request::is('admin/campaigns/*/logo') ? 'active' : ''}}"><a href="/admin/campaigns/{{$campaign->id}}/logo">Logo</a></li>
    <li role="presentation" class="{{Request::is('admin/campaigns/*/images') ? 'active' : ''}}"><a href="/admin/campaigns/{{$campaign->id}}/images">Image</a></li>
    <li role="presentation" class="{{Request::is('admin/campaigns/*/video') ? 'active' : ''}}"><a href="/admin/campaigns/{{$campaign->id}}/video">Video</a></li>
    <li role="presentation" class="{{Request::is('admin/campaigns/*/coupons') ? 'active' : ''}}"><a href="/admin/campaigns/{{$campaign->id}}/coupons">Coupons</a></li>
    <li role="presentation" class="{{Request::is('admin/campaigns/*/tracking') ? 'active' : ''}}"><a href="/admin/campaigns/{{$campaign->id}}/tracking">Tracking/Analytics</a></li>
    <li role="presentation" class="{{Request::is('admin/campaigns/*/lists/*') ? 'active' : ''}}"><a href="/admin/campaigns/{{$campaign->id}}/lists/mailchimp">Mailing List(s)</a></li>
    <li role="presentation" class="{{Request::is('admin/campaigns/*/sources') ? 'active' : ''}}"><a href="/admin/campaigns/{{$campaign->id}}/sources">Sources</a></li>
</ul>