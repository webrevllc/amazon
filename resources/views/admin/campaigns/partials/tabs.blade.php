<ul class="nav nav-tabs" role="tablist">
    <li class="{{Request::is('admin/campaigns') ? 'active' : ''}}"><a href="/admin/campaigns">Launched ({{count($total_campaigns->where('status', 'LAUNCHED'))}})</a></li>
    <li class="{{Request::is('admin/campaigns-ready') ? 'active' : ''}}"><a href="/admin/campaigns-ready">Ready To Launch ({{count($total_campaigns->where('status', 'READY TO LAUNCH'))}})</a></li>
    <li class="{{Request::is('admin/campaigns-paused') ? 'active' : ''}}"><a href="/admin/campaigns-paused">Paused ({{count($total_campaigns->where('status', 'PAUSED'))}})</a></li>
    <li class="{{Request::is('admin/campaigns-needs') ? 'active' : ''}}"><a href="/admin/campaigns-needs">Needs Coupons ({{count($total_campaigns->where('status', 'NEEDS COUPONS'))}})</a></li>
    <li class="{{Request::is('admin/campaigns-trashed') ? 'active' : ''}}"><a href="/admin/campaigns-trashed">Trashed ({{$trashed_count}})</a></li>
</ul>