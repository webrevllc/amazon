<table class="table table-bordered table-hover" id="step1">
    <thead>
    <tr>
        <th>Delete?</th>
        <th>Name</th>
        <th>Promo Url</th>
        <th id="step2">Coupons</th>
        <th id="step3">Status</th>
        <th id="step4">Edit</th>
        <th>View</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($campaigns as $campaign)
        <tr>
            <td ><a class="btn btn-sm btn-danger btn-block" href="/admin/campaigns/{{$campaign->id}}/delete"><i class="fa fa-trash text-center"></i></a></td>
            <td>{{$campaign->campaign_name}}</td>
            <td><a target="_blank" href="https://junglecoupon.com/coupon/{{$campaign->promo_url}}">https://junglecoupon.com/coupon/{{$campaign->promo_url}}</a></td>
            <td><a href="/admin/campaigns/{{$campaign->id}}/coupons">{{$campaign->claimed}}/{{count($campaign->coupons)}}</a></td>
            @if($campaign->status == "NEEDS COUPONS")
                <td><a href="/admin/coupons/{{$campaign->id}}">{{$campaign->status}}</a></td>
            @elseif($campaign->status == "READY TO LAUNCH")
                <td><a href="/admin/campaigns/{{$campaign->id}}/launch">READY TO LAUNCH</a></td>
            @elseif($campaign->status == "LAUNCHED")
                <td><a href="/coupon/{{$campaign->promo_url}}">LAUNCHED</a></td>
            @elseif($campaign->status == "PAUSED")
                <td><a href="/admin/campaigns/{{$campaign->id}}/launch">PAUSED, RE-LAUNCH?</a></td>
            @else
                <td></td>
            @endif
            <td>
                <a class="btn btn-sm btn-primary btn-block" href="/admin/campaigns/{{$campaign->id}}/edit"><i class="fa fa-pencil text-center"></i></a>
            </td>
            <td>
                <a class="btn btn-sm btn-info btn-block" href="/admin/campaigns/{{$campaign->id}}"><i class="fa fa-eye text-center"></i></a>
            </td>

            <td>
                @if($campaign->status == "READY TO LAUNCH")
                    <a class="btn btn-sm btn-danger btn-block" href="/admin/campaigns/{{$campaign->id}}/launch"><i class="fa fa-rocket text-center"></i></a>
                @elseif($campaign->status == "LAUNCHED")
                    <a class="btn btn-sm btn-danger btn-block" href="/admin/campaigns/{{$campaign->id}}/pause"><i class="fa fa-pause text-center"></i></a>
                @elseif($campaign->status == "PAUSED")
                    <a class="btn btn-sm btn-danger btn-block" href="/admin/campaigns/{{$campaign->id}}/launch"><i class="fa fa-rocket text-center"></i></a>
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>