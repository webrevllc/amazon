@extends('admin.layout')


@section('content')
    <div class="container spark-screen">
        <div class="row">
            <!-- Tabs -->
            <div class="col-md-4">
                <div class="panel panel-info panel-flush">
                    <div class="panel-heading">
                        Settings
                    </div>
                    <div class="panel-body">
                        <div class="spark-settings-tabs">
                            @include('admin.campaigns.partials.navbar')
                        </div>
                    </div>
                </div>
            </div>

            <!-- Tab Panes -->
            <div class="col-md-8">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        {{$campaign->campaign_name}}
                    </div>

                    <div class="panel-body">
                        @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $err)
                                        <li>{{$err}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="col-md-3">
                            <a  href="/admin/campaigns/{{$campaign->id}}/lists/mailchimp">Mailchimp</a><br>
                            <a  href="/admin/campaigns/{{$campaign->id}}/lists/aweber">Aweber</a><br>
                            <a  href="/admin/campaigns/{{$campaign->id}}/lists/csv">Download Excel</a>
                        </div>

                        <div class="col-md-9">

                            <div class="clearfix"></div>
                            @if(!empty($lists))
                                <form action="/admin/campaigns/{{$campaign->id}}/lists/aweber/list" method="post" class="top-space-lg">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label for="mc_api_key">Select which list to save emails to:</label>
                                        <select name="aweber" class="form-control">
                                            @foreach($lists as $list)
                                                <option value="{{$list['id']}}">{{$list['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-warning btn-lg pull-right">Select This List</button>
                                </form>
                            @else
                                <a href="/admin/campaigns/{{$campaign->id}}/authorizeaweber" class="btn btn-warning pull-right">Click Here To Use Aweber</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
