@extends('admin.layout')
@inject('C', 'App\Campaign')

@section('content')
    <div class="container spark-screen">
        <div class="row">
            <!-- Tabs -->
            <div class="col-md-4">
                <div class="panel panel-info panel-flush">
                    <div class="panel-heading">
                        Settings
                    </div>
                    <div class="panel-body">
                        <div class="spark-settings-tabs">
                            @include('admin.campaigns.partials.navbar')
                        </div>
                    </div>
                </div>
            </div>

            <!-- Tab Panes -->
            <div class="col-md-8">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        {{$campaign->campaign_name}}
                    </div>
                    <div class="panel-body">
                        <form method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="retargeting_pixel">Add Retargeting Pixel:</label>
                                <textarea name="retargeting_pixel" id="retargeting_pixel" class="form-control" rows="5">{{$campaign->retargeting_pixel}}</textarea>
                                <p>Paste in your favorite retargeting pixel, this will be added to your landing page so that you can retarget your customers.</p>
                            </div>
                            <div class="form-group">
                                <label for="pixel">Add Facebook Conversion Pixel:</label>
                                <textarea name="pixel" id="pixel" class="form-control" rows="5">{{$campaign->pixel}}</textarea>
                                <p>Paste in your Facebook conversion pixel, this will be added to your customers coupon page (once they claim it).</p>
                            </div>
                            <div class="form-group">
                                <label for="pixel">Add Google Analytics Tracking Code:</label>
                                <textarea name="analytics" id="pixel" class="form-control" rows="5">{{$campaign->analytics}}</textarea>
                                <p>Paste in your Google Analytics tracking code snippet (the whole thing). This will be added to your landing page.</p>
                            </div>
                            <div class="text-center top-space bottom-space-xl">
                                <button type="submit" class="btn btn-warning pull-right btn-lg ">Update Campaign</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

