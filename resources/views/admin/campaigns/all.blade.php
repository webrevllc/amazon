@extends('admin.layout')
<style>
    .btn-group{
        top: 20px;
    }
</style>
@section('content')
    <header id="top" class="sub-header">
        <div class="container">
            <div class="page-header">
                <h1>All of your campaigns
                    <small><a href="/admin/campaigns/create" class="btn btn-warning btn-xs "><i class="fa fa-plus"></i> Create a Campaign</a></small></h1>
            </div>
        </div>
        <!-- end .container -->
    </header>
    <div class="container">
        @include('admin.campaigns.partials.tabs')
        @if(count($campaigns) > 0 )
            @include('admin.campaigns.partials.campaign_table')
        @else
            <div class="container">
                <section class="footer-action">
                    <div class="page-header">
                        <h1>This page is empty.</h1>
                    </div>
                </section>
            </div>

        @endif
    </div>

@stop




