@extends('admin.layout')

@section('content')
    <div class="container spark-screen">
        <div class="row">
            <!-- Tabs -->
            <div class="col-md-4">
                <div class="panel panel-info panel-flush">
                    <div class="panel-heading">
                        Settings
                    </div>
                    <div class="panel-body">
                        <div class="spark-settings-tabs">
                            @include('admin.campaigns.partials.navbar')
                        </div>
                    </div>
                </div>
            </div>

            <!-- Tab Panes -->
            <div class="col-md-8">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        {{$campaign->campaign_name}}
                    </div>
                    <div class="panel-body">
                        <div class="alert alert-success">
                            <h4 style="margin:0px;">New! Now you can setup multiple links to drive traffic to and track how many coupon codes are attributed to each link!</h4>
                        </div>
                        @if(count($campaign->sources) > 0)
                            <table class="table table-bordered">
                                <th>URL</th>
                                <th># Visits/Clicks</th>
                                <th># Coupons Distributed</th>
                            @foreach($campaign->sources as $source)
                                <tr>
                                    <td>{{$source->url}}</td>
                                    <td>{{$source->visits}}</td>
                                    <td>{{$source->count}}</td>
                                </tr>
                            @endforeach
                            </table>
                        @else
                        <form method="post" action="/admin/campaigns/{{$campaign->id}}/sources">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="qty">How many unique urls do you want for this campaign?</label><br>
                                <input id="qty" name="qty" type="number" min="1" class="form-control"/>
                            </div>
                            <div class="text-center top-space bottom-space-xl">
                                <button type="submit" class="btn btn-warning pull-right btn-lg ">Add Sources</button>
                            </div>
                        </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

