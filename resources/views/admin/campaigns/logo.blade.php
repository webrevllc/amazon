@extends('admin.layout')

@section('content')
    <style>
        ul.thumbnails.image_picker_selector li{width:20%;}
    </style>
    <div class="container spark-screen">
        <div class="row">
            <!-- Tabs -->
            <div class="col-md-4">
                <div class="panel panel-info panel-flush">
                    <div class="panel-heading">
                        Settings
                    </div>
                    <div class="panel-body">
                        <div class="spark-settings-tabs">
                            @include('admin.campaigns.partials.navbar')
                        </div>
                    </div>
                </div>
            </div>

            <!-- Tab Panes -->
            <div class="col-md-8">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        {{$campaign->campaign_name}}
                    </div>
                    <div class="panel-body">
                        @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $err)
                                        <li>{{$err}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <p class="alert alert-danger">Click below to upload your logo or drag your logo into the box.</p>
                        <div class="col-md-6">
                            <ul>
                                <li>Logo will be resized to 300px wide.</li>
                                <li>Logo must be less than 3MB.</li>
                                <li>Accepted types are png, jpg, jpeg and bmp.</li>
                                <li>Only 1 logo allowed.</li>
                                <li>To change, simply drag a new logo into the box below.</li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            @if($campaign->campaign_logo_url != '')
                                <div>
                                    <img src="{{$campaign->campaign_logo_url}}" width="300" class="center-block" />
                                </div>

                                <div class="alert alert-info text-center">This is how your current logo will appear in the emails distributed to your customers.</div>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                        <form class="dropzone" id="addLogoForm" action="/admin/campaigns/{{$campaign->id}}/emaillogo" method="post">
                            {{csrf_field()}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop

@section('footer')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name="_token"]').attr('content')
            }
        });
        Dropzone.options.addLogoForm = {
            paramName: 'logo',
            maxFilesize: 3,
            acceptedFiles: '.jpg, .jpeg, .png, .bmp',
            maxFiles: 1,
            thumbnailWidth: 300,
            init: function() {
                this.on("success", function(file) { location.reload();  });
            }
        }
    </script>
@stop
