@extends('admin.layout')


@section('content')
    <header id="top" class="sub-header">
        <div class="container">
            <h3 class="page-title wow fadeInDown">{{Auth::user()->name}}</h3>
            <h5>{{Auth::user()->email}}</h5>
        </div>
        <!-- end .container -->
    </header>
    <div class="container top-space-lg">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#account" aria-controls="home" role="tab" data-toggle="tab">Account</a></li>
            <li role="presentation"><a href="#subscription" aria-controls="subscription" role="tab" data-toggle="tab">Subscription</a></li>
            <li role="presentation"><a href="#payments" aria-controls="payments" role="tab" data-toggle="tab">Payment History</a></li>
            <li role="presentation"><a href="#creditcard" aria-controls="creditcard" role="tab" data-toggle="tab">Credit Card</a></li>
            <li role="presentation"><a href="#cancel" aria-controls="cancel" role="tab" data-toggle="tab">Cancel</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="account">
                @include('admin.partials.account')
            </div>
            <div role="tabpanel" class="tab-pane fade" id="subscription">
                @include('admin.partials.subscription')
            </div>
            <div role="tabpanel" class="tab-pane fade" id="payments">
                @include('admin.partials.payments')
            </div>
            <div role="tabpanel" class="tab-pane fade" id="creditcard">
                @include('admin.partials.creditcard')
            </div>
            <div role="tabpanel" class="tab-pane fade" id="cancel">
                @include('admin.partials.cancel')
            </div>
        </div>

    </div>
@stop

@section('footer')
{{--<script>--}}
    {{--$('#myTabs a').click(function (e) {--}}
        {{--e.preventDefault()--}}
        {{--$(this).tab('show')--}}
    {{--})--}}
{{--</script>--}}
@stop