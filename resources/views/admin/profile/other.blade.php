@extends('admin.layout')

@section('content')
    <header id="top" class="sub-header">
        <div class="container">
            <h3 class="page-title wow fadeInDown">{{Auth::user()->name}}</h3>
            <h5>{{Auth::user()->email}}</h5>
        </div>
        <!-- end .container -->
    </header>
    <div class="container top-space-lg">
        @include('admin.profile.nav')

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="account">

                <p style="margin-top:40px;">
                    Want to integrate with MailChimp?
                </p>
                <form class="form-horizontal" style="margin-top:40px;" method="post" action="/admin/profile/other">
                    {{csrf_field()}}
                    <fieldset>
                        <div class="form-group">
                            <label for="api_key" class="col-sm-2 control-label">API Key?</label>
                            <div class="col-sm-10">
                                <input id="api_key" type="text" name="api_key" class="form-control" value="{{Auth::user()->mc_api_key}}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-danger">Add API Key</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>

    </div>
@stop





