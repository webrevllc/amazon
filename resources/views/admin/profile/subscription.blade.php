@extends('admin.layout')

@section('content')

    @include('admin.profile.header')

    <div class="container top-space-lg">
        @include('admin.profile.nav')
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="account">

                @if(! \Auth::user()->subscribed())
                    <h3>Your current subscription has been cancelled, update your credit card information to re-subscribe.</h3><br>
                    <form method="POST" action="/admin/profile/resubscribe" accept-charset="UTF-8" id="billing-form">

                        {{csrf_field()}}
                                <!-- Credit Card Number -->

                        <div class="form-group row">
                            <label for="cc-number" class="col-md-3 control-label">Credit Card Number:</label>

                            <div class="col-md-8">
                                <input type="text" id="cc-number" class="form-control input-md cc-number" data-stripe="number" placeholder="**** **** **** {{Auth::user()->getLastFourCardDigits()}}" required>
                            </div>
                        </div>

                        <!-- Expiration Date -->
                        <div class="form-group row">
                            <label class="col-md-3 control-label">Expiration Date:</label>
                            <div class="col-md-3">
                                {!! Form::selectMonth(null, null, ['data-stripe' => 'exp-month', 'class' => 'form-control col-md-6']) !!}
                            </div>
                            <div class="col-md-2 no-left-arm">
                                {!! Form::selectYear(null, date('Y'), date('Y') + 10, null, ['data-stripe' => 'exp-year', 'class' => 'form-control col-md-6']) !!}
                            </div>
                        </div>

                        <!-- CVV Number -->
                        <div class="form-group row">
                            <label for="cvv" class="col-md-3 control-label">CVV Number:</label>
                            <div class="col-md-3">
                                <input type="text" id="cvv" placeholder="" class="form-control input-md cvc" data-stripe="cvc" required>
                            </div>
                        </div>
                        <div class="payment-errors col-md-8" style="display:none"></div>
                        <button type="submit" class="btn btn-warning">Update Credit Card</button>

                    </form>
                @else
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="account">
                            <h2>Are you sure?</h2>

                            <h4>Do you really want to cancel your account?</h4>
                            <form method="POST" action="/admin/profile/cancel">
                                {{csrf_field()}}
                                <input type="hidden" name="yes" value="yes"/>
                                <button type="submit" class="btn btn-danger">Yes, I am sure.</button>
                            </form>
                        </div>
                    </div>
                @endif
            </div>
        </div>

    </div>
@stop

@section('footer')
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript" src="/js/jquery.min.js"></script>

    <script type="text/javascript">
        (function(){
            var StripeBilling = {
                init: function(){
                    this.form = $('#billing-form');
                    this.submitButton = this.form.find('input[type=submit]');
                    this.submitButtonValue = this.submitButton.val();
                    var stripeKey = $('meta[name="publishable-key"]').attr('content');
                    Stripe.setPublishableKey(stripeKey);
                    this.bindEvents();
                },
                bindEvents: function(){
                    this.form.on('submit', $.proxy(this.sendToken, this));
                },
                sendToken: function(event){
                    this.submitButton.val('One Moment').prop('disabled', true);
                    Stripe.createToken(this.form, $.proxy(this.stripeResponseHandler, this));
                    event.preventDefault();
                },
                stripeResponseHandler: function(status, response){
                    console.log(status, response);
                    if(response.error){
                        this.form.find('.payment-errors').show().html(response.error.message);
                        return this.submitButton.prop('disabled', false).val(this.submitButtonValue);
                    }
                    var token = response.id;
                    console.log(token);
                    this.form.append($('<input type="hidden" name="stripeToken" />').val(token));
                    this.form.get(0).submit();
                }
            };
            StripeBilling.init();
        })();

    </script>
@stop



