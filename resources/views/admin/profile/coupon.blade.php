


@extends('admin.layout')

@section('content')

    @include('admin.profile.header')

    <div class="container top-space-lg">
        <!-- Nav tabs -->

        @include('admin.profile.nav')

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="account">
                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $err)
                                <li>{{$err}}</li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                @endif
                <div class="col-md-offset-2 col-md-10">
                    <p style="margin-top:40px;">
                        If you have a coupon, enter it here, and we'll apply it to your next billing cycle.
                    </p>
                </div>

                <form method="POST" action="/admin/profile/coupon" accept-charset="UTF-8" >

                    {{csrf_field()}}
                    <div class="form-group row">
                        <label for="coupon" class="col-md-2 control-label">Coupon Code:</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control "  id="coupon" name="coupon" required>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-warning">Apply Coupon</button>
                </form>
            </div>
        </div>
    </div>
@stop
