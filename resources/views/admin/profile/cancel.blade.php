


@extends('admin.layout')

@section('content')
    @include('admin.profile.header')
    <div class="container top-space-lg">
        @include('admin.profile.nav')

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="account">
                <h2>Are you sure?</h2>

                <h4>Do you really want to cancel your account?</h4>
                <form method="POST">
                    {{csrf_field()}}
                    <input type="hidden" name="yes" value="yes"/>
                    <button type="submit" class="btn btn-lg btn-danger">Yes, I am sure.</button>
                </form>
            </div>
        </div>
    </div>
@stop






