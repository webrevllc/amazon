


@extends('admin.layout')

@section('content')

    @include('admin.profile.header')

    <div class="container top-space-lg">
        @include('admin.profile.nav')

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="account">
                <h3>Payment History</h3>
                <table class="table table-bordered">
                    <thead>
                    <th>Payment Date</th>
                    <th>Amount</th>
                    <th>Invoice</th>
                    </thead>
                        @foreach (Auth::user()->invoices() as $invoice)
                            <tr>
                                <td>{{ $invoice->dateString() }}</td>
                                <td>{{ $invoice->dollars() }}</td>
                                <td><a href="/user/invoice/{{ $invoice->id }}">Download</a></td>
                            </tr>
                        @endforeach
                </table>
            </div>
        </div>
    </div>
@stop




