


@extends('admin.layout')

@section('content')

    @include('admin.profile.header')

    <div class="container top-space-lg">
        @include('admin.profile.nav')

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="account">

             <h4 style="margin-top:40px;">
                    Want to update the credit card that we have on file? Provide the new details here. Don't worry; your card information will never touch our servers.
             </h4>

                <form method="POST" action="/admin/profile/credit_card" accept-charset="UTF-8" id="billing-form">

                    {{csrf_field()}}
                            <!-- Credit Card Number -->
                    <div class="form-group row">
                        <label for="cc-number" class="col-md-3 control-label">Credit Card Number:</label>

                        <div class="col-md-8">
                            <input type="text" id="cc-number" class="form-control input-md cc-number" data-stripe="number" placeholder="**** **** **** {{Auth::user()->getLastFourCardDigits()}}" required>
                        </div>
                    </div>

                    <!-- Expiration Date -->
                    <div class="form-group row">
                        <label class="col-md-3 control-label">Expiration Date:</label>
                        <div class="col-md-3">
                            {!! Form::selectMonth(null, null, ['data-stripe' => 'exp-month', 'class' => 'form-control col-md-6']) !!}
                        </div>
                        <div class="col-md-2 no-left-arm">
                            {!! Form::selectYear(null, date('Y'), date('Y') + 10, null, ['data-stripe' => 'exp-year', 'class' => 'form-control col-md-6']) !!}
                        </div>
                    </div>

                    <!-- CVV Number -->
                    <div class="form-group row">
                        <label for="cvv" class="col-md-3 control-label">CVV Number:</label>
                        <div class="col-md-3">
                            <input type="text" id="cvv" placeholder="" class="form-control input-md cvc" data-stripe="cvc" required>
                        </div>
                    </div>
                    <div class="payment-errors col-md-8" style="display:none"></div>
                    <button type="submit" class="btn btn-warning">Update Credit Card</button>

                </form>
            </div>
        </div>
    </div>
@stop
@section('footer')
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript" src="/js/jquery.min.js"></script>

    <script type="text/javascript">
        (function(){
            var StripeBilling = {
                init: function(){
                    this.form = $('#billing-form');
                    this.submitButton = this.form.find('input[type=submit]');
                    this.submitButtonValue = this.submitButton.val();
                    var stripeKey = $('meta[name="publishable-key"]').attr('content');
                    Stripe.setPublishableKey(stripeKey);
                    this.bindEvents();
                },
                bindEvents: function(){
                    this.form.on('submit', $.proxy(this.sendToken, this));
                },
                sendToken: function(event){
                    this.submitButton.val('One Moment').prop('disabled', true);
                    Stripe.createToken(this.form, $.proxy(this.stripeResponseHandler, this));
                    event.preventDefault();
                },
                stripeResponseHandler: function(status, response){
                    console.log(status, response);
                    if(response.error){
                        this.form.find('.payment-errors').show().html(response.error.message);
                        return this.submitButton.prop('disabled', false).val(this.submitButtonValue);
                    }
                    var token = response.id;
                    console.log(token);
                    this.form.append($('<input type="hidden" name="stripeToken" />').val(token));
                    this.form.get(0).submit();
                }
            };
            StripeBilling.init();
        })();

    </script>
@stop