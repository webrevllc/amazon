<ul class="nav nav-tabs" role="tablist">
    <li class="{{set_active('admin/profile/account')}}"><a href="/admin/profile/account" aria-controls="home" >Account</a></li>
    <li class="{{set_active('admin/profile/subscription')}}"><a href="/admin/profile/subscription" aria-controls="subscription" >Subscription</a></li>
    <li class="{{set_active('admin/profile/payments')}}"><a href="/admin/profile/payments" aria-controls="payments" >Payment History</a></li>
    <li class="{{set_active('admin/profile/creditcard')}}"><a href="/admin/profile/creditcard" aria-controls="creditcard" >Credit Card</a></li>
    <li class="{{set_active('admin/profile/coupon')}}"><a href="/admin/profile/coupon">Coupon</a></li>
{{--    <li class="{{set_active('admin/profile/cancel')}}"><a href="/admin/profile/cancel" aria-controls="cancel" >Cancel</a></li>--}}
</ul>