@extends('admin.layout')

@section('content')
    <div class="container">
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            This section will get you started with JungleCoupon. If you cannot find an answer to your question,
            make sure to <a href="mailto:help@junglecoupon.com">contact us</a>.
        </div>

        <div class="panel-group" id="accordion">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">What is JungleCoupon?</a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <p>JungleCoupon is the answer to a widely known problem that Amazon retailers have.</p>

                        <p>In 2015 Amazon.com allowed its retailers the ability to create "single-use coupons". While this did resolve some other issues that retailers faced it posed new issues:</p>

                        <ul><li><p>Retailers are now responsible for creating an attractive landing page to promote their product and a form to gather an email address in order to send the single-use coupon to.</p></li>
                            <li><p>Single-use coupon distribution is not scalable, retailers have to resort to emailing coupons one at a time to their potential customers, who has time for that?</p></li>
                            <li><p>Retailers want to protect their merchandise, good standing with Amazon.com and inventory, but this just created a whole new job!</p></li>
                        </ul><blockquote>
                            <p>JungleCoupon allows retailers to <strong>VERY QUICKLY</strong> generate a campaign,
                                limit who receives coupons for their merchandise, and collect valuable customer information.</p>
                        </blockquote>
                    </div>
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">Editing Your Campaign</a>
                    </h4>
                </div>
                <div id="collapseTen" class="panel-collapse collapse">
                    <div class="panel-body">
                        <p>Once you have created your Campaign you will be able to edit that Campaign. JungleCoupon will always try to improve this area of our tool as it is the most crucial part.</p>
                        <p>From here you have the ability to add several tools to your Campaign in order to increase its success.</p>
                        <p>You can currently configure your Campaign with the following options:</p>
                        <ul><li>Campaign Name</li>
                            <li>Promo Title (this is publicly visible</li>
                            <li>Show product reviews from Amazon on your landing page?</li>
                            <li>Allow only 1 coupon to be distributed to per IP address (basically this is like 1 coupon per household)</li>
                            <li>Filter known disposable emails - with a database of over 7,000 known disposable and spam email domains (constantly growing) we can ensure your Campaign will not get spammed.</li>
                            <li>Regular price comes from Amazon based on your ASIN</li>
                            <li>Promo Price - what will your customers pay for this product?</li>
                            <li>Promotion End Date</li>
                            <li>Tracking - this area allows you to add retargeting pixels and analytics to your landing page, and also a conversion pixel to the coupon page (when a customer actually receives the coupon).</li>
                            <li>Mailing lists (we will synchronize with Mailchimp, Constant Contact and Aweber in the near future) this allows you to add all email addresses that are assigned to a coupon to be added to the list of your choosing.</li>
                            <li>Featured image will display on your landing page.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven">Adding Coupons</a>
                    </h4>
                </div>
                <div id="collapseEleven" class="panel-collapse collapse">
                    <div class="panel-body">
                        <p>Once your Campaign is created and you have edited it to your liking, before you can view the landing page you must upload your coupons.</p>

                        <p>Simply click the <strong>Manage Coupons</strong> link at the top of your campaign to go to the Coupon Management screen.</p>

                        <p>At first you will not have any coupons here, so:</p>

                        <ul><li>Click the Add Coupons button</li>
                            <li>Click "Browse"</li>
                            <li>Find your coupon .txt file that Amazon gives you when you generate your single use coupons.</li>
                            <li>Then click upload and you're done!</li>
                        </ul><p><strong>Now you can view your Campaign!</strong></p>
                    </div>
                </div>
            </div>


            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Houston, We Are Go For Launch</a>
                    </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse">
                    <div class="panel-body">
                        <p>Once you have edited your campaign to your liking and uploaded all of your coupons it is time to launch your Campaign.</p>
                        <p>This can be done easily in 2 different areas.</p>
                        <ul><li>From the Campaign preview area</li>
                            <li>From your Campaign list</li>
                        </ul><p>In your Campaign preview area simply click the "Launch" link at the top.</p>
                        <p>In you Campaigns list either click "READY TO LAUNCH" or the shuttle icon on the right.</p>
                    </div>
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Pausing</a>
                    </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse">
                    <div class="panel-body">
                        <p>If at any time while you campaign is launched, simply click the pause icon in your Campaign list for that Campaign.</p>
                        <p>This will take down your Campaign and any traffic that lands on your landing page will see a message stating your Campaign has ended.</p>
                        <p>See the attached image.</p>
                        <p>If you wish to re-launch, simply click the shuttle icon or the "Paused, re-launch?" link.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <style>
        .faqHeader {
            font-size: 27px;
            margin: 20px;
        }

        .panel-heading [data-toggle="collapse"]:after {

            float: right;
            color: #F58723;
            font-size: 18px;
            line-height: 22px;
            /* rotate "play" icon from > (right arrow) to down arrow */
            -webkit-transform: rotate(-90deg);
            -moz-transform: rotate(-90deg);
            -ms-transform: rotate(-90deg);
            -o-transform: rotate(-90deg);
            transform: rotate(-90deg);
        }

        .panel-heading [data-toggle="collapse"].collapsed:after {
            /* rotate "play" icon from > (right arrow) to ^ (up arrow) */
            -webkit-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            -ms-transform: rotate(90deg);
            -o-transform: rotate(90deg);
            transform: rotate(90deg);
            color: #454444;
        }
    </style>

@stop