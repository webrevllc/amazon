@extends('newlayout')

@section('content')
    <div class="container " id="contact">
        <div class="section-title">
            <h1>Jungle Coupon</h1>
            <p>Sign Up!</p>
        </div>

        <div class="contact-form col-md-8 col-md-offset-2 bottom-space-xl ">
            <form method="POST" accept-charset="UTF-8" id="signupform">

                {{csrf_field()}}
                <div class="form-group row">
                    <div class="payment-errors alert alert-danger" style="display:none; width:100%;"></div>
                </div>

                <div class="form-group row">
                    <label for="plan" class=" col-md-4 control-label">Which Plan:</label>
                    <div class="col-md-8">
                        <select name="plan" id="plan" class="form-control">
                            <option value="jungle_monthly">Monthly ($39.95/month)</option>
                            <option value="jungle_q">Quarterly ($109.85/every 3 months)</option>
                            <option value="jungle_annual">Yearly ($399.95/year)</option>
                        </select>
                    </div>
                </div>
                        <!-- Credit Card Number -->
                <div class="form-group row">
                    <label for="cc-number" class="col-md-4 control-label">Credit Card Number:</label>

                    <div class="col-md-8">
                        <input type="text" id="cc-number" class="form-control input-md cc-number" data-stripe="number" placeholder="**** **** **** ****" >
                    </div>
                </div>

                <!-- Expiration Date -->
                <div class="form-group row">
                    <label class="col-md-4 control-label">Expiration Date:</label>
                    <div class="col-md-4">
                        {!! Form::selectMonth(null, null, ['data-stripe' => 'exp-month', 'class' => 'form-control col-md-6']) !!}
                    </div>
                    <div class="col-md-4 no-left-arm">
                        {!! Form::selectYear(null, date('Y'), date('Y') + 10, null, ['data-stripe' => 'exp-year', 'class' => 'form-control col-md-6']) !!}
                    </div>
                </div>

                <!-- CVV Number -->
                <div class="form-group row">
                    <label for="cvv" class="col-md-4 control-label">CVV Number:</label>
                    <div class="col-md-8">
                        <input type="text" id="cvv" placeholder="" class="form-control input-md cvc" data-stripe="cvc" >
                    </div>
                </div>
                <div class="form-group row">
                    <label for="coupon" class="col-md-4 control-label">Have A Coupon?</label>
                    <div class="col-md-8">
                        <input type="text" id="coupon" name="coupon" class="form-control input-md cvc" >
                    </div>
                </div>
                <button  type="submit" class="btn btn-danger btn-block btn-lg">Subscribe</button>
            </form>
        </div>
    </div>
    <div class="clearfix"></div>


@stop
@section('footer')
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript" src="/js/jquery.min.js"></script>

    <script type="text/javascript">
        (function(){
            var StripeBilling = {
                init: function(){
                    this.form = $('#signupform');
                    this.submitButton = this.form.find('input[type=submit]');
                    this.submitButtonValue = this.submitButton.val();
                    var stripeKey = $('meta[name="publishable-key"]').attr('content');
                    Stripe.setPublishableKey(stripeKey);
                    this.bindEvents();
                },
                bindEvents: function(){
                    this.form.on('submit', $.proxy(this.sendToken, this));
                },
                sendToken: function(event){
                    this.submitButton.val('One Moment').prop('disabled', true);
                    Stripe.createToken(this.form, $.proxy(this.stripeResponseHandler, this));
                    event.preventDefault();
                },
                stripeResponseHandler: function(status, response){
                    console.log(status, response);
                    if(response.error){
                        this.form.find('.payment-errors').show().html(response.error.message);
                        return this.submitButton.prop('disabled', false).val(this.submitButtonValue);
                    }
                    var token = response.id;
                    console.log(token);
                    this.form.append($('<input type="hidden" name="stripeToken" />').val(token));
                    this.form.get(0).submit();
                }
            };
            StripeBilling.init();
        })();

    </script>
@stop



