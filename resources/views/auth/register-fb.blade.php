@extends('newlayout')
@section('pixel')
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
        fbq('init', '509538945889028');
        fbq('track', 'InitiateCheckout');
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=509538945889028&ev=PageView&noscript=1" /></noscript>
@stop
@section('content')
<style>
    .panel-plan .panel-heading{background:#e44948; color:#ffffff;}
    .panel-success .panel-heading{background:#00CC66; color:#ffffff;}
    .panel-info .panel-heading{background:#0099ff; color:#ffffff;}
    .btn-info{background:#00CC66; color:#ffffff;}
    body {
        background: url(/img/gray-clouds.png) no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }
</style>
<nav class="navbar navbar-default ">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><img src="/img/toplogo.png" alt="JungleCoupon"> </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">

            <ul class="nav navbar-nav navbar-right">
                <li><a href="/login">Login</a></li>
                <li class="active"><a href="/register">Sign Up</a></li>
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
</nav>
    <div class="container " id="contact">
        <div class="col-md-8 col-md-offset-2">
            <div class="row header text-center" style="margin-bottom:20px;">
                {{--<h4>JungleCoupon is free for 7 days.</h4>--}}
                <h6>If you don't like it, cancel your account. <br>It's as easy as that.</h6>
            </div>
        </div>
        <div class="contact-form bottom-space-xl ">
            <form role="form" method="post"  id="signupform">
                {{csrf_field()}}
                <div class="col-md-8 col-md-offset-2 ">
                    @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $err)
                                    <li>{{$err}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <legend>Create Your Account</legend>
                    <div class="form-group row">
                        <label for="Name" class="col-md-4 control-label">Name:</label>
                        <div class="col-md-8">
                            <input class="form-control " id="Name" data-stripe="name"  name="name" type="text" required value="{{$u->name}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="control-label col-md-4">Email:</label>
                        <div class="col-md-8">
                            <input class="form-control" id="email"  name="email" required value="{{$u->email}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="control-label col-md-4">Password:</label>
                        <div class="col-md-8">
                            <input type="password" name="password" id="password" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password_confirmation" class="control-label col-md-4">Confirm Password:</label>
                        <div class="col-md-8">
                            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" required>
                        </div>
                        <input type="hidden" value="JUNGLEMONTH" name="plan">
                    </div>
                    <div class="form-group row">
                        <div class="payment-errors alert alert-danger" style="display:none; width:100%;"></div>
                    </div>
                    <!-- Credit Card Number -->
                    <div class="form-group row">
                        <label for="cc-number" class="col-md-4 control-label">Credit Card Number:</label>
                        <div class="col-md-8">
                            <input type="text" id="cc-number" class="form-control input-md cc-number" data-stripe="number" placeholder="**** **** **** ****" required>
                        </div>
                    </div>
                    <!-- Expiration Date -->
                    <div class="form-group row">
                        <label class="col-md-4 control-label">Expiration Date:</label>
                        <div class="col-md-4">
                            {!! Form::selectMonth(null, null, ['data-stripe' => 'exp-month', 'class' => 'form-control col-md-6']) !!}
                        </div>
                        <div class="col-md-4 no-left-arm">
                            {!! Form::selectYear(null, date('Y'), date('Y') + 10, null, ['data-stripe' => 'exp-year', 'class' => 'form-control col-md-6']) !!}
                        </div>
                    </div>
                    <!-- CVV Number -->
                    <div class="form-group row">
                        <label for="cvv" class="col-md-4 control-label">CVV Number:</label>
                        <div class="col-md-8">
                            <input type="text" id="cvv" placeholder="" class="form-control input-md cvc" data-stripe="cvc" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="coupon" class="col-md-4 control-label">Have A Coupon?</label>
                        <div class="col-md-8">
                            <input type="text" id="coupon" name="coupon" class="form-control input-md cvc" >
                        </div>
                    </div>
                    <div class="row">
                        <div class=" col-md-8 col-md-offset-4" style="margin-bottom: 40px;">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="terms" required> I agree to JungleCoupon's <a target="_blank" href="/terms">Terms Of Service</a>
                                </label>
                            </div>
                            <button  type="submit" class="btn btn-info "><i class="fa fa-check-square-o"></i> Sign Up Now</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop
@section('footer')
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript" src="/js/jquery.min.js"></script>

    <script type="text/javascript">
        (function(){
            var StripeBilling = {
                init: function(){
                    this.form = $('#signupform');
                    this.submitButton = this.form.find('input[type=submit]');
                    this.submitButtonValue = this.submitButton.val();
                    var stripeKey = $('meta[name="publishable-key"]').attr('content');
                    Stripe.setPublishableKey(stripeKey);
                    this.bindEvents();
                },
                bindEvents: function(){
                    this.form.on('submit', $.proxy(this.sendToken, this));
                },
                sendToken: function(event){
                    this.submitButton.val('One Moment').prop('disabled', true);
                    Stripe.createToken(this.form, $.proxy(this.stripeResponseHandler, this));
                    event.preventDefault();
                },
                stripeResponseHandler: function(status, response){
                    console.log(status, response);
                    if(response.error){
                        this.form.find('.payment-errors').show().html(response.error.message);
                        return this.submitButton.prop('disabled', false).val(this.submitButtonValue);
                    }
                    var token = response.id;
                    console.log(token);
                    this.form.append($('<input type="hidden" name="stripeToken" />').val(token));
                    this.form.get(0).submit();
                }
            };
            StripeBilling.init();
        })();

    </script>
@stop