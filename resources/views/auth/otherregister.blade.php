@extends('newlayout')
@section('content')
    <div class="container" id="contact">
        <div class="section-title">
            <h1>Jungle Coupon</h1>
            <p>Sign Up!</p>
        </div>

        <div class="contact-form bottom-space-xl ">
            <form role="form" method="post" action="/register" id="signupform">
                {{csrf_field()}}


                <input type="hidden" name="plan" value="{{session('plan')}}"/>
                <div class="col-md-8 col-md-offset-2">
                    @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $err)
                                    <li>{{$err}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <fieldset>
                        <legend>Account Information</legend>

                        <div class="form-group">
                            <label for="Name" class="control-label">Name:</label>
                            <input class="form-control" id="Name"  name="name" type="text">
                        </div>
                        <div class="form-group">
                            <label for="email" class="control-label">Email:</label>
                            <input class="form-control" id="email"  name="email" type="email">
                        </div>
                        <div class="form-group">
                            <label for="password" class="control-label">Password:</label>
                            <input type="password" name="password" id="password" class="form-control" >
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation" class="control-label">Confirm Password:</label>
                            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" >
                        </div>
                    </fieldset>
                    <br>
                    <fieldset>
                        <legend>Billing Info</legend>
                        <div class="alert alert-danger payment-errors" role="alert" style="display:none;"></div>
                        <div class="form-group">
                            <label class="control-label">Which Plan:</label>
                            <select name="plan" id="plan" class="form-control" onchange="amountChanged(this)">
                                <option value="jungle_monthly">Monthly ($39.95/month)</option>
                                <option value="jungle_quarterly">Quarterly ($89.95/every 3 months)</option>
                                <option value="jungle_annual">Yearly ($399.95/year)</option>
                            </select>
                        </div>
                    </fieldset>
                </div>
            </form>
            <div class="col-md-8 col-md-offset-2">
                <div class="form-group buttons">
                    <div class="text-center top-space">
                        <button  class="btn btn-success btn-block btn-lg" id="stripe">Sign Up</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop
@section('footer')


    <script src="https://checkout.stripe.com/checkout.js"></script>
    <script>
        var jungleAmount = '3995';
        var amountChanged = function(sel){

            switch (sel.value) {
                case 'jungle_monthly':
                    jungleAmount = '3995';
                    break;
                case 'jungle_quarterly':
                    jungleAmount = '8995';
                    break;
                case 'jungle_annual':
                    jungleAmount = '39995';
                    break;
            }
        };
        var handler = StripeCheckout.configure({
            key: "{{env('STRIPE_KEY')}}",
            image: "https://stripe.com/img/documentation/checkout/marketplace.png",
            name: "JungleCoupon.com",
            panelLabel: "Subscribe",
            allowRememberMe: true,
            amount: jungleAmount,
            zipCode: true,
            token: function(token){
                $('form#signupform').append($('<input type="hidden" name="stripeToken" />').val(token.id));
                console.log(token);
            }
        });

        document.getElementById('stripe').addEventListener('click', function(e) {
            handler.open();
            e.preventDefault();
        });
    </script>
@stop