@extends('newlayout')
@section('content')
    <style>
        .panel-plan .panel-heading{background:#e44948; color:#ffffff;}
        body {
            background: url(/img/gray-clouds.png) no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
    </style>
    <nav class="navbar navbar-default ">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><img src="/img/toplogo.png" alt="JungleCoupon"> </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">

                <ul class="nav navbar-nav navbar-right">
                    <li ><a href="/login">Login</a></li>
                    <li ><a href="/register">Sign Up</a></li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </nav>
    <div class="container" id="contact">
        <div class="section-title">
            <h1>Jungle Coupon</h1>
            <p>Password Recovery</p>
        </div>

        <div class="contact-form bottom-space-xl ">
            <div class="col-md-8 col-md-offset-2">
                <form method="POST" action="/password/email">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label for="email" class="control-label">Email:</label>
                        <input class="form-control" id="email" required name="email" type="email" value="{{ old('email') }}">
                    </div>
                    <div>
                        <button type="submit" class="btn btn-lg btn-danger btn-block">
                            Send Password Reset Link
                        </button>
                    </div>
                </form>
            </div>
        </div>

    </div>

@stop



