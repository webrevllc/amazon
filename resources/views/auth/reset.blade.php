@extends('newlayout')
@section('content')
    <div class="container" id="contact">
        <div class="section-title">
            <h1>Jungle Coupon</h1>
            <p>Password Reset</p>
        </div>
        <div class="contact-form bottom-space-xl ">
            <div class="col-md-8 col-md-offset-2">
                <form method="POST" action="/password/reset">
                    {!! csrf_field() !!}
                    <input type="hidden" name="token" value="{{ $token }}">
                    <div class="form-group">
                        <label for="email" class="control-label">Email:</label>
                        <input class="form-control" id="email" required name="email" type="email" value="{{ old('email') }}">
                    </div>
                    <div class="form-group">
                        <label for="password" class="control-label">Password:</label>
                        <input class="form-control" id="password" required name="password" type="password">
                    </div>
                    <div class="form-group">
                        <label for="password_confirmation" class="control-label">Password:</label>
                        <input class="form-control" id="password_confirmation" required name="password_confirmation" type="password">
                    </div>
                    <div>
                        <button type="submit" class="btn btn-lg btn-danger btn-block">
                            Reset Password
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @stop



