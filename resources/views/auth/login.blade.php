@extends('newlayout')
@section('content')
    <style>
        .panel-plan .panel-heading{background:#e44948; color:#ffffff;}
        body {
            background: url(/img/gray-clouds.png) no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
    </style>
    <nav class="navbar navbar-default ">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><img src="/img/toplogo.png" alt="JungleCoupon"> </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">

                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="/login">Login</a></li>
                    <li ><a href="/register">Sign Up</a></li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </nav>
    <div class="container" id="contact">
        <div class="section-title">
            <h1>Jungle Coupon</h1>
            <p>Welcome back, please sign in!</p>
        </div>

        <div class="contact-form bottom-space-xl ">
            <form role="form" method="post" action="/login">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="row">
                            {{csrf_field()}}
                            @if(count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $err)
                                            <li>{{$err}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="form-group">
                                <label for="email" class="control-label">Email:</label>
                                <input class="form-control" id="email" required name="email" type="email">
                            </div>
                            <div class="form-group">
                                <label for="password" class="control-label">Password:</label>
                                <input type="password" name="password" id="password" class="form-control" required>
                            </div>
                            <div class="form-group text-left">
                                <input type="checkbox" name="remember" id="remember"> <label for="remember">Remember Me</label>
                            </div>
                            <div class="form-group buttons">
                                <div class="text-center top-space">
                                    <button type="submit" class="btn btn-success btn-block btn-lg" id="js-contact-btn">Login</button>
                                    {{--<a href="/login/facebook" class="btn btn-info btn-block btn-lg" id="js-contact-btn">Login With Facebook</a>--}}
                                    <a href="/password/email">Forgot Password?</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@stop