@extends('newlayout')

@section('pixel')
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');
        fbq('init', '509538945889028');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=509538945889028&ev=PageView&noscript=1" /></noscript>
@stop

@section('content')


<nav class="navbar navbar-default navbar-fixed-top ">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><img src="img/toplogo.png" alt="JungleCoupon"> </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">

            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="#top">Home</a></li>
                <li><a href="#questions">Questions</a></li>
                <li><a href="#pricing">Pricing</a></li>
                <li><a href="#contact">Contact</a></li>
                <li><a href="/login">Login</a></li>
                <li><a href="/register">Sign Up</a></li>
                {{--<li><a class="btn btn-info" style="color:#fff;" href="/login/facebook">Sign Up With Facebook</a></li>--}}
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
</nav>
<!-- // End Fixed navbar -->

<header id="top" class="header top-space-lg">
    <div class="container">
        <div class="header_top-bg">
            <div class="logo">
                <img src="img/logo1.png" alt="JungleCoupon" style="max-width: 80%;" class="center-block">
            </div>
        </div>

        <h3 class="headline-support wow fadeInDown">JUNGLECOUPON</h3>
        <h1 class="headline wow fadeInDown" data-wow-delay="0.1s">MANAGE. AMAZON. COUPONS. </h1>

        <div class="header_bottom-bg">
            <a class="btn btn-default btn-xl wow zoomIn" style="white-space: normal;" data-wow-delay="0.3s" href="https://junglecoupon.com/register" >SIGN UP NOW</a>
            <a href="/login" ><p class="cta_urgency wow fadeIn" data-wow-delay="0.5s"><small><i class="fa fa-share"></i> Login</small></p></a>
        </div>

    </div>
    <!-- end .container -->
</header>
<!-- end .header -->

<!--
 Highlight Section
 ====================================== -->

<section class="highlight">
    <div class="container">
        <h2 class="lead text-center">JungleCoupon is the most cost efficient way to deliver your Amazon coupons to potential customers.</h2>
    </div>
    <!-- end .container -->

</section>
<!-- end section.highlight -->

<section class="benefits">
    <div class="container">
        <div class="section-title wow fadeInUp">
            <h4>HOW IT WORKS</h4>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="row text-center bottom-space-lg" >
                    <button type="button" class="btn btn-danger btn-lg " data-toggle="modal" data-target="#demoVideo">
                        Watch Demo Video Now!
                    </button>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="benefit-item wow fadeInLeft">
                            <div class="benefit-icon">1.</div>
                            <h6 class="benefit-title">Get Your Coupons!</h6>
                            <p>Simply log into your Amazon Seller Account, and create a batch of single use coupons. Download them as a text file.</p>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="benefit-item wow fadeInRight">
                            <div class="benefit-icon">2.</div>
                            <h6 class="benefit-title">Login!</h6>
                            <p><a href="https://junglecoupon.com/register">Sign up</a> and login to you JungleCoupon.com account and create a new campaign!</p>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="benefit-item wow fadeInLeft">
                            <div class="benefit-icon">3.</div>
                            <h6 class="benefit-title">Upload Your Coupons!</h6>
                            <p>Once you have created your campaign, simply upload your coupon file and design your landing page.</p>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="benefit-item wow fadeInRight">
                            <div class="benefit-icon">4.</div>
                            <h6 class="benefit-title">Launch!</h6>
                            <p>Once you have designed your landing page we will generate a unique link for you to share with the world. New customers add their email address and we send them a coupon!</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <!-- end .container -->
</section>
<!-- end section.benefits -->
<hr class="top-space-lg">
<div class="container">
    <div class="section-title wow fadeInUp">
        <h4>BOOST SALES, GET MORE REVIEWS, AND DON'T LOSE YOUR SHIRT</h4>
    </div>

    {{--<div class="nav-center bottom-space-lg">--}}
        {{--<!-- Nav tabs -->--}}
        {{--<ul class="nav nav-pills" role="tablist" id="schedule-tabs">--}}
            {{--<li role="presentation" class="active"><a href="#tab-1" aria-controls="tab-1" role="tab" data-toggle="tab">Coupon Management</a></li>--}}
            {{--<li role="presentation"><a href="#tab-2" aria-controls="tab-2" role="tab" data-toggle="tab">Coupon Tracking</a></li>--}}
            {{--<li role="presentation"><a href="#tab-3" aria-controls="tab-3" role="tab" data-toggle="tab">Retargeting Customers</a></li>--}}
        {{--</ul>--}}

    {{--</div>--}}

    {{--<div class="tab-content">--}}
        {{--<div role="tabpanel" class="tab-pane active" id="tab-1">--}}
            {{--<p class="lead text-center">Coupon Management</p>--}}
            {{--<p>Once you set up a coupon in Seller Central, you'll realize you have very little control over redemption - because you can't set quantity limits for your coupon. Why is this a problem? If someone shares your coupon on Facebook or SlickDeals - bargain hunters can wipe out you entire inventory in a matter of hours, leaving you with a hefty fulfilment bill to pay. </p>--}}
        {{--</div>--}}
        {{--<!-- end .tabpanel -->--}}

        {{--<div role="tabpanel" class="tab-pane" id="tab-2">--}}
            {{--<p class="lead text-center">Coupon Tracking</p>--}}
            {{--<p>Our system allows you to insert your Facebook Conversion Pixel directly onto your coupon's landing page, as well as the user's thank you page. Watch your coupons turn into sales every step of the way.</p>--}}
        {{--</div>--}}
        {{--<!-- end .tabpanel -->--}}

        {{--<div role="tabpanel" class="tab-pane" id="tab-3">--}}
            {{--<p class="lead text-center">Retargeting Customers</p>--}}
            {{--<p> It's common business practice to market to existing customers - e.g. to give loyalty discount or to announce new products - simply because they're more likely to buy from you again than a new customer.--}}

                {{--However, Amazon does NOT want you to do this. They mask buyers' emails so everything you send gets logged in their system. If you get caught sending marketing material to your buyers, you run the risk of getting banned forever (it's a violation to their terms of services). Easily integrate your campaign with your favorite MailList client and keep your customers for yourself! </p>--}}
        {{--</div>--}}
        {{--<!-- end .tabpanel -->--}}

    {{--</div>--}}
</div>
<section class="faq-accordion highlight" id="questions">
    <div class="container">
        <div class="section-title wow fadeInUp">
            <h4>FREQUENTLY ASKED QUESTIONS</h4>
        </div>
        <div class="row">
            <div >
                <!--
                 Left Accordion Starts
                 ===================== -->
                <div class="panel-group" id="accordion-1" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="how">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion-1" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                    How does JungleCoupon work?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false">
                            <div class="panel-body">
                                Once you sign up with JungleCoupon you will create your first campaign. We ask for the Amazon ASIN as well as a few other questions that will enable us to create your landing page for you. Once you load up your coupons and launch the campaign you drive traffic to a unique url that you create. Potential customers will add their email address and receive a "claim code" via email. Once your soon to be new customers click that code they are taken to a coupon page that will give them your coupon and a link directly to your product.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="landing">
                            <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    What do the landing pages look like?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false">
                            <div class="panel-body">
                                We have set up a <a href="https://junglecoupon.com/coupon/keurig-40-off" title="JungleCoupon Demo Page">demo landing page</a>, check it out! Plus - we are planning to roll out multiple layouts and themes to choose from in the near future.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    How many campaigns can I run at once?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false">
                            <div class="panel-body">
                                Currently our system is setup so that you can run as many campaigns and promote as many of your products simultaneously.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapseThree-a" aria-expanded="false" aria-controls="collapseThree-a">
                                    Can I add my Super URL to the coupon claim page?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThree-a" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree-a" aria-expanded="false">
                            <div class="panel-body">
                                JungleCoupon is still investigating the validity and weight of Super URL's and determining which, if any, methods of structuring the URL are best. <br><br>In the meantime we will open the ability for users to paste in <i>whatever</i> URL you want. If you do not choose to paste in your own URL, the default URL will be chosen for you.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                            <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Can I add my Amazon reviews to my landing page?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false">
                            <div class="panel-body">
                                Absolutely! We make that feature optional (in case you don't have many reviews yet).
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne2">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" href="#collapseOne-3" data-parent="#accordion-1" aria-expanded="false" aria-controls="collapseOne-2">
                                    What email list providers do you integrate with?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne-3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                            <div class="panel-body">
                                Aweber integration was completed on 09/10/15 along with MailChimp. Now whenever someone claims one of your coupons they are added to the list of your choice. Use a different provider? No problem just download the CSV/Excel once your campaign is done and upload them wherever you want :)
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne2">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" href="#collapseOne-2" data-parent="#accordion-1" aria-expanded="false" aria-controls="collapseOne-2">
                                    Can I use JungleCoupon in Canada/UK?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne-2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                            <div class="panel-body">
                                Currently we are only setup to work ASIN's for the US. If you enter a Canadian ASIN, the product MAY match the US ASIN, but not likely. Canadian/UK support will be coming soon.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo2">
                            <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" href="#collapseTwo-2" data-parent="#accordion-1" aria-expanded="false" aria-controls="collapseTwo-2">
                                    Can I add my Facebook Conversion Pixel to the landing pages?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo-2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo2">
                            <div class="panel-body">
                                Yes! In fact, JungleCoupon allows you to paste in your Google analytics tracking snippet, any conversion pixel you want as well as a retargeting pixel. Simply paste it into your campaign and save the changes, we take care of the rest.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree2">
                            <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" href="#collapseThree-2" aria-expanded="false" data-parent="#accordion-1" aria-controls="collapseThree-2">
                                    What if I run out of coupons for my campaign, can I add more?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThree-2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree2">
                            <div class="panel-body">
                                Yup! Simply go to your Coupon Management page and upload more coupons!
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end left accordion // .panel-group -->
            </div>
        </div>

    </div>
</section>
<!-- end .tab-content -->
{{--<section class="embed_twitter">--}}
    {{--<div class="container">--}}
        {{--<div class="section-title wow fadeInUp">--}}
            {{--<h4>WHAT PEOPLE SAY</h4>--}}
        {{--</div>--}}

        {{--<div class="row">--}}
            {{--<div class="col-md-4">--}}
                {{--<div class="testblock">I can't believe I was spending so much with those other guys!</div>--}}
                {{--<div class="clientblock"> <img src="/images/client3.jpg" alt=".">--}}
                    {{--<p><strong>John W.</strong>--}}
                        {{--<br>San Antonio, TX</p>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-4">--}}
                {{--<div class="testblock">Easy to use, easy to manage my customers, what more could you ask for?</div>--}}
                {{--<div class="clientblock"> <img src="/images/client2.jpg" alt=".">--}}
                    {{--<p><strong>Shane R.</strong>--}}
                        {{--<br>Orlando, FL</p>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-4">--}}
                {{--<div class="testblock">I sold out 500 pieces of my inventory and lost money before I found JungleCoupon - what a life saver!</div>--}}
                {{--<div class="clientblock"> <img src="/images/client1.jpg" alt=".">--}}
                    {{--<p><strong>Steven W.</strong>--}}
                        {{--<br>Charlotte, NC</p>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<!-- end .container -->--}}
{{--</section>--}}
<!-- end section.sponsors -->


<section class="pricing" id="pricing">

    <div class="container">

        <div class="section-title">
            <h4>PRICING</h4>
            {{--<p>All Of Our Plans Have a 7-Day Free Trial</p>--}}
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="pricing-item highlighted wow zoomIn" data-wow-duration="2s">
                    <div class="plan-name">Monthly Plan</div>
                    <div class="price"> <span class="curr">$</span>39.95</div>

                    <ul class="plan-features">
                        <li> Unlimited Campaigns </li>
                        <li> Unlimited Coupons</li>
                        <li> Instant Access</li>
                    </ul>
                    <a class="btn btn-outline btn-lg" href="https://junglecoupon.com/register">Go Monthly</a>
                </div>

            </div>
            {{--<div class="col-md-4">--}}
                {{--<div class="pricing-item highlighted-plan wow zoomIn" data-wow-delay="0.3s" data-wow-duration="2s">--}}
                    {{--<div class="plan-name">Quarterly Plan</div>--}}
                    {{--<div class="price"> <span class="curr">$</span>109.85</div>--}}

                    {{--<ul class="plan-features">--}}
                        {{--<li> Unlimited Campaigns </li>--}}
                        {{--<li> Unlimited Coupons</li>--}}
                        {{--<li> Instant Access</li>--}}
                        {{--<li> Save The Cost Of One Month Over The Course Of A Year!</li>--}}
                    {{--</ul>--}}
                    {{--<a class="btn btn-success btn-lg" href="https://junglecoupon.com/register">Go Quarterly</a>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-4">--}}
                {{--<div class="pricing-item wow zoomIn" data-wow-delay="0.6s" data-wow-duration="2s">--}}
                    {{--<div class="plan-name">Annual Plan</div>--}}
                    {{--<div class="price"> <span class="curr">$</span>399.95</div>--}}
                    {{--<ul class="plan-features">--}}
                        {{--<li> Unlimited Campaigns </li>--}}
                        {{--<li> Unlimited Coupons</li>--}}
                        {{--<li> Instant Access</li>--}}
                        {{--<li> Save The Cost Of Two Months Over The Course Of A Year!</li>--}}
                    {{--</ul>--}}
                    {{--<a class="btn btn-outline btn-lg" href="https://junglecoupon.com/register">Go Annual</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
</section>
<!-- end section.pricing -->


<!--
 Mailchimp Subscribe
 ====================================== -->



<!-- //  end .highlight -->



<div class="container" id="contact">
    <div class="section-title">
        <h5>HAVE QUESTIONS??? ASK AWAY!</h5>
        <p>If you have any questions about JungleCoupon, please contact us directly. We will respond as quickly as possible.</p>
    </div>

    <div class="contact-form bottom-space-xl wow fadeInUp">

        <form action="/message" method="POST">
            {{csrf_field()}}
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>First Name</label>
                                <input type="text" class="form-control" name="first_name" placeholder="First Name" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" class="form-control" name="last_name" placeholder="last name" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Email Address</label>
                        <input type="email" class="form-control" name="email" placeholder="Email Address" required>
                    </div>
                    <div class="form-group">
                        <label>Phone Number</label>
                        <input type="text" class="form-control" name="phone" placeholder="Optional">
                    </div>
                    <div class="form-group">
                        <label>Your Message</label>
                        <textarea class="form-control" name="message" rows="6" placeholder="Enter your message here" required> </textarea>
                    </div>

                    <div class="text-center top-space">
                        <button type="submit" class="btn btn-success btn-block btn-lg" id="js-contact-btn">Send Message</button>
                        <div id="js-contact-result" data-success-msg="Form submitted successfully." data-error-msg="Oops. Something went wrong."></div>
                    </div>
                </div>
            </div>
        </form>
    </div>

</div>
<!-- // end .container -->
<div class="highlight">
    <div class="container">
        <div class="row">
            <form action="/email" method="post" class="form subscribe-form" id="">
                {{csrf_field()}}
                <div class="form-group col-md-3 hidden-sm">
                    <h6 class="susbcribe-head wow fadeInLeft"> SUBSCRIBE <small>TO OUR NEWSLETTER</small></h6>
                </div>
                <div class="form-group col-sm-8 col-md-6 wow fadeInRight">
                    <label class="sr-only">Email address</label>
                    <input type="email" class="form-control input-lg" placeholder="Enter your email" name="email" required>
                </div>
                <div class="form-group col-sm-4 col-md-3">
                    <button type="submit" class="btn btn-lg btn-danger btn-block">Subscribe Now!</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--
 Footer Call to Action
 ====================================== -->

<section class="footer-action">
    <div class="container">
        <h4 class="headline-support wow fadeInDown">JungleCoupon</h4>
        <h2 class="headline wow fadeInDown" data-wow-delay="0.1s">JOIN THE REVOLUTION</h2>
        <div class="footer_bottom-bg">
            <a class="btn btn-success btn-xl wow zoomIn" style="white-space: normal;" data-wow-delay="0.3s" href="/register">SIGN UP NOW!</a>
        </div>
    </div>
</section>
<!-- end section.footer-action -->
<footer>
    {{--<div class="social-icons">--}}
        {{--<a href="#" class="wow zoomIn"> <i class="fa fa-twitter"></i> </a>--}}
        {{--<a href="#" class="wow zoomIn" data-wow-delay="0.2s"> <i class="fa fa-facebook"></i> </a>--}}
        {{--<a href="#" class="wow zoomIn" data-wow-delay="0.4s"> <i class="fa fa-linkedin"></i> </a>--}}
    {{--</div>--}}
    <p> <small class="text-muted">Copyright &copy; {{date('Y')}}. All rights reserved. JungleCoupon.com</small></p>

</footer>

<a href="#top" class="back_to_top"><img src="images/back_to_top.png" alt="back to top"></a>

<div class="modal fade" id="demoVideo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">JungleCoupon Demo</h4>
            </div>
            <div class="modal-body">
                <video width="100%" controls>
                    <source src="/videos/JCDemo3.mp4" type="video/mp4">
                    <source src="/videos/JCDemo3.ogg" type="video/ogg">
                    Your browser does not support HTML5 video.
                </video>
            </div>
            <div class="modal-footer">
                <a href="/register"  class="btn btn-danger" style="white-space: normal;">SIGN UP NOW!</a>
            </div>
        </div>
    </div>
</div>

@stop