@extends('newlayout')

@section('content')
    <div class="container" style="margin-top:50px;">
        <div class="well">
            <div class="page-header">
                <h1>Privacy Policy</h1>
            </div>
            <p>This Privacy Policy governs the manner in which WebRev LLC collects, uses, maintains and discloses information collected from users (each, a &#8220;User&#8221;) of the JungleCoupon.com website (&#8220;Site&#8221;). This privacy policy applies to the Site and all products and services offered by WebRev LLC.</p>
            <p><strong>Personal identification information</strong></p>
            <p>We may collect personal identification information from Users in a variety of ways, including, but not limited to, when Users visit our site, register on the site, place an order, subscribe to the newsletter, respond to a survey, fill out a form, and in connection with other activities, services, features or resources we make available on our Site. Users may be asked for, as appropriate, name, email address, mailing address, phone number, credit card information. Users may, however, visit our Site anonymously. We will collect personal identification information from Users only if they voluntarily submit such information to us. Users can always refuse to supply personally identification information, except that it may prevent them from engaging in certain Site related activities.</p>
            <p><strong>Non-personal identification information</strong></p>
            <p>We may collect non-personal identification information about Users whenever they interact with our Site. Non-personal identification information may include the browser name, the type of computer and technical information about Users means of connection to our Site, such as the operating system and the Internet service providers utilized and other similar information.</p>
            <p><strong>Web browser cookies</strong></p>
            <p>Our Site may use &#8220;cookies&#8221; to enhance User experience. User&#8217;s web browser places cookies on their hard drive for record-keeping purposes and sometimes to track information about them. User may choose to set their web browser to refuse cookies, or to alert you when cookies are being sent. If they do so, note that some parts of the Site may not function properly.</p>
            <p><strong>How we use collected information</strong></p>
            <p>WebRev LLC may collect and use Users personal information for the following purposes:</p>
            <ul>
                <li><em>&#8211; To improve customer service</em><br />
                    Information you provide helps us respond to your customer service requests and support needs more efficiently.</li>
                <li><em>&#8211; To personalize user experience</em><br />
                    We may use information in the aggregate to understand how our Users as a group use the services and resources provided on our Site.</li>
                <li><em>&#8211; To improve our Site</em><br />
                    We may use feedback you provide to improve our products and services.</li>
                <li><em>&#8211; To send periodic emails</em><br />
                    We may use the email address to send User information and updates pertaining to their order. It may also be used to respond to their inquiries, questions, and/or other requests. If User decides to opt-in to our mailing list, they will receive emails that may include company news, updates, related product or service information, etc. If at any time the User would like to unsubscribe from receiving future emails, they may do so by contacting us via our Site.</li>
            </ul>
            <p><strong>How we protect your information</strong></p>
            <p>We adopt appropriate data collection, storage and processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction of your personal information, username, password, transaction information and data stored on our Site.</p>
            <p>Sensitive and private data exchange between the Site and its Users happens over a SSL secured communication channel and is encrypted and protected with digital signatures.</p>
            <p><strong>Sharing your personal information</strong></p>
            <p>We do not sell, trade, or rent Users personal identification information to others. We may share generic aggregated demographic information not linked to any personal identification information regarding visitors and users with our business partners, trusted affiliates and advertisers for the purposes outlined above.</p>
            <p><strong>Changes to this privacy policy</strong></p>
            <p>WebRev LLC has the discretion to update this privacy policy at any time. When we do, we will revise the updated date at the bottom of this page. We encourage Users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.</p>
            <p><strong>Your acceptance of these terms</strong></p>
            <p>By using this Site, you signify your acceptance of this policy. If you do not agree to this policy, please do not use our Site. Your continued use of the Site following the posting of changes to this policy will be deemed your acceptance of those changes.<br />
                This document was last updated on November 03, 2014
            </p>
        </div>
    </div>
@stop