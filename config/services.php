<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],

    'ses' => [
        'key'    => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model'  => App\User::class,
        'key'    => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'github' => [
        'client_id' => 'c351365b36569c256eb2',
        'client_secret' => '4b60f1938769694a196e4ccb2dc4c9b4a9373995',
        'redirect' => 'http://localhost:8000/login',
    ],

    'facebook' => [
        'client_id' => '854962661266088',
        'client_secret' => '350049bde36fea2d6c7efed2503daaae',
        'redirect' => 'https://junglecoupon.com/login/callback/facebook',
//        'redirect' => 'http://localhost:8000/login/callback/facebook',
    ],
    'campaign_monitor' => [
        'key' => env('CAMPAIGN_MONITOR_KEY')
    ]
];
